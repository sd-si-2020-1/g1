# Visão Geral

Este documento visa esclarecer quais são os limites da aplicação. 

Nossa aplicação de receitas tem como foco permitir que um usuário visualize todas as receitas disponíveis no app e, também, criar uma lista de compras pessoal com os ingredientes presentes nas receitas.