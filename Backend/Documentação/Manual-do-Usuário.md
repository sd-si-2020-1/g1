#9 **GUIA PARA CONFIGURAÇÃO E INSTALAÇÃO DO APP**

**Softwares necessários:**

- Back-end: [Node](https://nodejs.org/en/download/)
- Sistema de Arquivos: [Docker](https://www.docker.com/)
- Front-end: [React Native](https://reactnative.dev/)
- Documentação da API: Open API com [Swagger](https://swagger.io/)
- Banco de dados: [MongoDB](https://www.mongodb.com/)
- Uso obrigatório de WebSockets

**Instalação**

**Passo 1:**

Baixe os arquivos do projeto:

```
git clone https://gitlab.com/sd-si-2020-1/g1.git`

```
Para os passos seguintes é necessário ter instalado em sua máquina o gerenciador de pacotes npm. Para mais informações consulte o site oficial deles: https://www.npmjs.com/get-npm.

**Passo 2:**

Execute o comando a seguir dentro da pasta app:

```
cd g1/app/

npm install

```

**Passo 3:**

Para executar a aplicação é necessário ter instalado em sua máquina o docker e docker compose. Para mais informações consulte o site oficial deles: https://docs.docker.com/get-docker/ e https://docs.docker.com/compose/install/.

Se você utiliza o sistema operacional Windows é necessário instalar a versão WSL 2 consulte mais informações em: [https://docs.microsoft.com/pt-br/windows/wsl/install-win10#step-6---install-your-linux-distribution-of-choice](https://docs.microsoft.com/pt-br/windows/wsl/install-win10#step-6---install-your-linux-distribution-of-choice) e [https://docs.microsoft.com/pt-br/windows/wsl/wsl2-kernel](https://docs.microsoft.com/pt-br/windows/wsl/wsl2-kernel)

**Execução**

Para executar a aplicação é necessário que tenha uma conta no Docker.

No prompt de comando digite:

```

docker Login

```

A seguinte mensagem deve aparecer:

```

Login with your Docker ID to push and pull images from Docker Hub. If you don't have a Docker ID, head over to https://hub.docker.com to create one.

Username:

Password:

```

Após digitar os dados de login a seguinte mensagem deve aparecer:

```

Login Succeeded

```

Execute o comando a seguir dentro do diretório app

```
docker-compose up

```

Para encerrar a aplicação execute o comando:


```
docker-compose stop

```

O servidor irá executar 

Para testar acesse o endereço:

```
Localhost:3000

```

Para executar o Swagger, execute no prompt o comando:

```
npm start 

```
dentro da pasta nodejs-swagger

Para visualizar a interface de IU do Swagger:

```
http://localhost:8080/docs

```