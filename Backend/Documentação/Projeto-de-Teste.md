# Visão Geral

Este documento visa esclarecer como os testes serão realizados.

Os casos de testes referentes aos requisitos funcionais se encontram no documento [**Casos de Teste**](Casos-de-Teste.md). É importante notar que os casos de teste foram construídos pensando nas interações possíveis  do usuário. Portanto, podemos reduzir o número de testes se reduzirmos o número de interações.

Parte dos testes, que envolverem requisições GET/POST, serão feitas utilizando o [Postman](https://www.postman.com/). Essas requisições se encontram neste [**documento**](https://receitas.postman.co/collections/6746459-7ca579d0-eebf-4bd2-836b-544d0d5cfefb?version=latest&workspace=e5bbf9cf-788c-4cde-8930-d96a14fbb3e3).

Parte dos testes serão automatizados, como os testes unitários e de integração, utilizando a ferramenta [Jest](https://jestjs.io/). Essa ferramenta foi escolhida ao invés do [Mocha](https://mochajs.org/) por ser mais simples de se configurar e, também, por ser própria para o React.

Os testes automatizados serão usados para configuração da CI/CD do Gitlab, permitindo, então, aplicarmos a integração contínua neste projeto. Dessa forma, todo e qualquer push para o repositório só serão aceitos caso eles passem por todos os testes automatizados.

