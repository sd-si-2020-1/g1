# Visão Geral

Neste diretório se encontram reunidos os principais documentos referentes à parte backend da aplicação.

- [**Requisitos funcionais**](Requisitos-Funcionais.md)
- [**Escopo**](Escopo.md)
- [**Projeto de Teste**](Projeto-de-Teste.md)
- [**Casos de Teste**](Casos-de-Teste.md)
- [**Arquitetura**](Arquitetura.png)
- [**Manual do usuário**](Manual-do-Usuário.md)
- [**Relatório de Testes**](test-report.html)

Observação: Para visualizar o [**Relatório de Testes**](test-report.html) é necessário baixar o arquivo e abrir ele com o seu navegador (Chrome, Firefox, Opera e etc).

Documentos auxiliares:

- Gerenciamento da equipe no [**Trello**](https://trello.com/invite/b/2Ux6ayRb/337283311081f672c50ef65fc7978981/projeto-de-sistemas-distribu%C3%ADdos).

- Consultas GET/POST feitas no Postman para auxiliar nos testes das rotas da API. Para acessar o [**documento**](https://receitas.postman.co/collections/6746459-7ca579d0-eebf-4bd2-836b-544d0d5cfefb?version=latest&workspace=e5bbf9cf-788c-4cde-8930-d96a14fbb3e3) entre primeiramente em nosso [**Time**](https://app.getpostman.com/join-team?invite_code=fa1af54d07b0eb6a92df973d246e2931&ws=e5bbf9cf-788c-4cde-8930-d96a14fbb3e3) do postman.

