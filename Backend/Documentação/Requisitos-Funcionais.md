# Visão Geral

Este documento visa listar todos os requisitos funcionais presentes na aplicação na forma de história de usuário. A nomeação dos requisitos segue o padrão "ReqSis"+ número de identificação. 



| ID | Requisito | Descrição| Status |
|----|-----------|----------|--------|
|ReqSis1 |  Criar usuário | Eu como usuário desejo ser capaz de criar uma conta na aplicação para ter acesso as receitas| Implementado :heavy_check_mark: | <!-- Separador para ajudar na vizualização -->
|ReqSis2 |  Login | Eu como usuário desejo ser capaz de acessar a aplicação informando apenas meus dados de usuário e senha | Implementado :heavy_check_mark:| <!-- Separador para ajudar na vizualização -->
|ReqSis3 |  Logout | Eu como usuário desejo ser capaz sair da aplicação | Implementado :heavy_check_mark: |<!-- Separador para ajudar na vizualização -->
|ReqSis4 |  Criar lista | Eu como usuário desejo ser capaz de criar uma lista de compras com os ingredientes que eu escolher | Implementado :heavy_check_mark:| <!-- Separador para ajudar na vizualização -->
|ReqSis5 |  Editar lista | Eu como usuário desejo ser capaz de editar minha lista de compras | Implementado :heavy_check_mark:| <!-- Separador para ajudar na vizualização -->
|ReqSis6 |  Excluir lista | Eu como usuário desejo ser capaz de excluir a minha lista de compras | Implementado :heavy_check_mark:|<!-- Separador para ajudar na vizualização -->
|ReqSis7 |  Adicionar ingrediente | Eu como usuário desejo ser capaz de adicionar novos ingredientes na minha lista | Implementado :heavy_check_mark:|<!-- Separador para ajudar na vizualização -->
|ReqSis8 |  Editar Ingrediente | Eu como usuário desejo ser capaz de editar o ingrediente que criei | Implementado :heavy_check_mark:| <!-- Separador para ajudar na vizualização -->
|ReqSis9 |  Remover ingrediente | Eu como usuário desejo ser capaz de remover ingredientes da minha lista | Implementado :heavy_check_mark:| <!-- Separador para ajudar na vizualização -->
|ReqSis10 |  Criar receita | Eu como usuário desejo ser capaz de criar novas receitas que serão vistas por outros usuários | Implementado :heavy_check_mark:| <!-- Separador para ajudar na vizualização -->
|ReqSis11 |  Editar Receita | Eu como usuário desejo ser capaz de editar as informações da receita que criei | Implementado :heavy_check_mark:| <!-- Separador para ajudar na vizualização -->
|ReqSis12 |  Excluir receita | Eu como usuário desejo ser capaz de excluir uma receita que eu criei | Implementado :heavy_check_mark:|<!-- Separador para ajudar na vizualização -->
|ReqSis13 |  Excluir Usuário | Eu como usuário desejo ser capaz de excluir minha própria conta | Implementado :heavy_check_mark: |





