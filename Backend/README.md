# Sobre

Este documento especifica os detalhes do **backend**.

# Sumário
- [Cronograma](#Cronograma)
- [Documentação](#Documentação)
- [Instalação](#Instalação)
- [Execução](#Execução)
- [Execução no Docker](#Execução-no-Docker)
- [Utilizando a API com Swagger](#Utilizando-a-API-com-Swagger)
- [Testes Automatizados](#Testes-Automatizados)
- [Contato](#Contato)

# Cronograma

- 08/10/20  - Entrega do primeiro Sprint
- 15/10/20 - Entrega do segundo Sprint
- 22/10/20 - Entrega do terceiro Sprint
- 29/10/20 - Entrega do quarto Sprint
- 05/11/20 - Entrega do quinto Sprint
- 12/11/20 - Entrega do sexto Sprint (último)
- 13/11/20 - Entrega de apresentação individual s/ sua auto-avaliação (texto) e participação em vídeo em 15 minutos (demonstrativo,  slides são opcionais)



# Documentação
A documentação dos outros artefatos gerados durante o desenvolvimento do backend se encontra neste [documento](/Backend/Documentação/README.md).


# Instalação
Para os passos seguintes é necessário ter instalado em sua máquina o gerenciador de pacotes **npm** e o banco de dados **mongoDB**. Para mais informações consulte o site oficial deles: https://www.npmjs.com/get-npm, https://www.mongodb.com/. 
```
git clone https://gitlab.com/sd-si-2020-1/g1.git

cd g1/Backend/app/

npm install
```


# Execução
```
npm start
```


# Execução no Docker
Alternativamente, é possível executar a aplicação usando o **docker compose** seguindo os comandos abaixo. Observe que, nesse caso, não é necessário ter o **npm** instalado e nem mesmo o **mongodb**. Para mais informações sobre a instalação do docker consulte o site oficial deles: https://docs.docker.com/get-docker/ e https://docs.docker.com/compose/install/
```
git clone https://gitlab.com/sd-si-2020-1/g1.git

cd g1/Backend/app/

/*Iniciar a aplicação*/
docker-compose up

/*Encerrar a aplicação*/
docker-compose stop
```

# Utilizando a API com Swagger
É possível testar as rotas da API com o uso do [Swagger](https://swagger.io/). Para isso não é necessário fazer nenhuma configuração extra, pois o swagger será executado junto com o backend em uma rota específica. Para acessá-la clique no seguinte link:
```
http://localhost:3000/api-docs/
```

# Testes Automatizados
Para executar os testes basta instalar os pacotes de desenvolvimento e usar o npm para chamar o script de teste de dentro da pasta app.
```
cd g1/Backend/app/

npm install && npm install --only=dev

npm test
```


# Contato

Qualquer dúvida ou sugestão, entre em contado com os [integrantes do projeto](#Integrantes-e-suas-responsabilidades), ou abra uma nova [issue](https://gitlab.com/sd-si-2020-1/g1/-/issues). 