const app = require("../app"); // Link to your server file
const request = require("supertest");
const mongoose = require("mongoose");
const User = require("../models/User");
const UserList = require("../models/UserList");
const Recipe = require("../models/Recipe");


const testUsername = 'JamesBond';
const testPassword = 'JamesBond123';

const wrongUsername = 'JamesNaoBond'
const wrongPassword = 'batata';

const recipeTitle = "Farofa";
const recipeDescription = "Bem gostosa e temperada!";

const ingredientName = "Farinha";
const ingredientDescription = "Farinha branca fina";
const ingredientCount = 3;
const ingredientUnit = "kg";

const itemName = "Feijão";
const itemDescription = "um pacote de feijao";
const itemCount = 1;

/*Store the recipeID, after its successfully creation to use in other tests */
let recipeID;

//Clear database before doing all tests
// beforeAll(async () => {
//   await User.deleteMany();
//   await UserList.deleteMany();
//   await Recipe.deleteMany();
// });


//Clear database after doing all tests and close connection
afterAll(async () => {
  await User.deleteMany();
  await UserList.deleteMany();
  await Recipe.deleteMany();

  mongoose.connection.close();
});

/*****************************************************************************************************/
describe("Database Initialization", () => {
  //Here we wait 5s to give the time necessary to database initialization
  it("Initialize database connection", async (done) => {
    expect(mongoose.connection.readyState).toBe(2);
    done();
  }, 5000);
});

/*****************************************************************************************************/

describe("User Management", () => {
  it("Create new User sucessfully", async (done) => {
    const response = await request(app)
      .post("/signup")
      .type("json")
      .send({ username: testUsername, password: testPassword });
    expect(response.status).toBe(200);
    const responseBody = response.body;
    expect(responseBody.success).toBe(true);
    done();
  });

  it("Try to create duplicate User", async (done) => {
    const response = await request(app)
      .post("/signup")
      .type("json")
      .send({ username: testUsername, password: testPassword });
    expect(response.status).toBe(500);
    const responseBody = response.body;
    expect(responseBody.err.name).toBe("UserExistsError");
    done();
  });

  it("Try to create User without providing username", async (done) => {
    const response = await request(app)
      .post("/signup")
      .type("json")
      .send({ password: testPassword });
    expect(response.status).toBe(500);
    const responseBody = response.body;
    expect(responseBody.err.name).toBe("MissingUsernameError");
    done();
  });

  it("Try to create User without providing password", async (done) => {
    const response = await request(app)
      .post("/signup")
      .type("json")
      .send({ username: testUsername });
    expect(response.status).toBe(500);
    const responseBody = response.body;
    expect(responseBody.err.name).toBe("UserExistsError");
    done();
  });
});


describe("Database Connection", () => {
  it("Connection with database is already established", async (done) => {
    expect(mongoose.connection.readyState).toBe(1);
    done();
  }, 5000);
});

/*****************************************************************************************************/

describe("Recipe Management", () => {
  describe("Recipe Creation", () => {
    /*We have to give user credentials, username and password,
      and recipe info, title and description, to create a recipe sucessfully.
      Obs: All recipes are created with no ingredients.
    */
    it("Create Recipe successfully", async (done) => {
      const response = await request(app)
        .post("/recipe")
        .type("json")
        .send({
          username: testUsername,
          password: testPassword,
          recipe_title: recipeTitle,
          recipe_description: recipeDescription
      });
      
      expect(response.status).toBe(200);
      const responseBody = response.body;
      expect(responseBody._id).toBeDefined();
      expect(responseBody.username).toBe(testUsername);
      expect(responseBody.recipe_title).toBe(recipeTitle);
      expect(responseBody.recipe_description).toBe(recipeDescription);
      expect(Array.isArray(responseBody.recipe_ingredients)).toBe(true);
      expect(responseBody.recipe_ingredients.length).toBe(0);
      /* Here we store the recipeID*/
      recipeID = responseBody.id;
      done();
    });

    it("Try to create Recipe with wrong password", async (done) => {
      const response = await request(app)
        .post("/recipe")
        .type("json")
        .send({
          username: testUsername,
          password: wrongPassword,
          recipe_title: recipeTitle,
          recipe_description: recipeDescription
      });
      expect(response.status).toBe(401);
      done();
    });

    it("Try to create Recipe with wrong username", async (done) => {
      const response = await request(app)
        .post("/recipe")
        .type("json")
        .send({
          username: wrongUsername,
          password: wrongPassword,
          recipe_title: recipeTitle,
          recipe_description: recipeDescription
      });
      expect(response.status).toBe(401);
      done();
    });

    it("Try to create Recipe without providing password", async (done) => {
      const response = await request(app)
        .post("/recipe")
        .type("json")
        .send({
          username: testUsername,
          recipe_title: recipeTitle,
          recipe_description: recipeDescription
      });
      expect(response.status).toBe(400);
      done();
    });

    it("Try to create Recipe without providing username", async (done) => {
      const response = await request(app)
        .post("/recipe")
        .type("json")
        .send({
          password: testPassword,
          recipe_title: recipeTitle,
          recipe_description: recipeDescription
      });
      expect(response.status).toBe(400);
      done();
    });

    it("Try to create Recipe with missing recipe title", async (done) => {
      const response = await request(app)
        .post("/recipe")
        .type("json")
        .send({
          username: testUsername,
          password: testPassword,
          recipe_description: recipeDescription
      });
      expect(response.status).toBe(500);
      done();
    });

    it("Try to create Recipe with missing recipe description", async (done) => {
      const response = await request(app)
        .post("/recipe")
        .type("json")
        .send({
          username: testUsername,
          password: testPassword,
          recipe_title: recipeTitle
      });
      expect(response.status).toBe(500);
      done();
    });
  });

  /**************************************************************************************************/

  describe("Recipe Read", () => {
    it("Read Recipe successfully", async (done) => {
      const response = await request(app).get("/recipe/"+ String(recipeID));
      
      expect(response.status).toBe(200);
      /*The API returns an Array of JSON objects, therefore we need to access the array with response.body[0] */
      const responseBody = response.body[0];
      expect(responseBody._id).toBeDefined();
      expect(responseBody.username).toBe(testUsername);
      expect(responseBody.recipe_title).toBe(recipeTitle);
      expect(responseBody.recipe_description).toBe(recipeDescription);
      expect(Array.isArray(responseBody.recipe_ingredients)).toBe(true);      
      done();
    });
  });

/**************************************************************************************************/

  describe("Adding ingredients", () => {
    it("Add ingredient successfully", async (done) => {
      const response = await request(app)
        .post("/recipe/"+ String(recipeID)+ "/ingredients")
        .type("json")
        .send({
          ingredient_name: ingredientName,
          ingredient_description: ingredientDescription,
          ingredient_count: ingredientCount,
          ingredient_unit: ingredientUnit,         
          username: testUsername,
          password: testPassword
      });
             
      const responseBody = response.body;
      //console.log("INGREDIENTS: " + JSON.stringify(responseBody));
      expect(response.status).toBe(200);
      expect(responseBody._id).toBeDefined();
      expect(responseBody.username).toBe(testUsername);
      expect(responseBody.recipe_title).toBe(recipeTitle);
      expect(responseBody.recipe_description).toBe(recipeDescription);

      expect(Array.isArray(responseBody.recipe_ingredients)).toBe(true);
      expect(responseBody.recipe_ingredients[0].ingredient_name).toBe(ingredientName);
      expect(responseBody.recipe_ingredients[0].ingredient_description).toBe(ingredientDescription);
      expect(responseBody.recipe_ingredients[0].ingredient_count).toBe(ingredientCount);   
      expect(responseBody.recipe_ingredients[0].ingredient_unit).toBe(ingredientUnit);          

      done();
    });

    it("Add ingredient with missing ingredient Description successfully", async (done) => {
      const response = await request(app)
        .post("/recipe/"+ String(recipeID)+ "/ingredients")
        .type("json")
        .send({
          ingredient_name: ingredientName,
          ingredient_count: ingredientCount,
          ingredient_unit: ingredientUnit,         
          username: testUsername,
          password: testPassword
      });
             
      const responseBody = response.body;
      //console.log("INGREDIENTS: " + JSON.stringify(responseBody));
      expect(response.status).toBe(200);
      expect(responseBody._id).toBeDefined();
      expect(responseBody.username).toBe(testUsername);
      expect(responseBody.recipe_title).toBe(recipeTitle);
      expect(responseBody.recipe_description).toBe(recipeDescription);

      /* Here the array is [1], because we have already inserted one item in the test above*/
      expect(Array.isArray(responseBody.recipe_ingredients)).toBe(true);
      expect(responseBody.recipe_ingredients[1].ingredient_name).toBe(ingredientName);
      expect(responseBody.recipe_ingredients[1].ingredient_description).toBeUndefined();
      expect(responseBody.recipe_ingredients[1].ingredient_count).toBe(ingredientCount);   
      expect(responseBody.recipe_ingredients[1].ingredient_unit).toBe(ingredientUnit);          

      done();
    });

    it("Try to add ingredient with missing ingredient Count successfully", async (done) => {
      const response = await request(app)
        .post("/recipe/"+ String(recipeID)+ "/ingredients")
        .type("json")
        .send({
          ingredient_name: ingredientName,
          ingredient_description: ingredientDescription,
          ingredient_unit: ingredientUnit,         
          username: testUsername,
          password: testPassword
      });
             
      const responseBody = response.body;
      //console.log("INGREDIENTS: " + JSON.stringify(responseBody));
      expect(response.status).toBe(200);
      expect(responseBody._id).toBeDefined();
      expect(responseBody.username).toBe(testUsername);
      expect(responseBody.recipe_title).toBe(recipeTitle);
      expect(responseBody.recipe_description).toBe(recipeDescription);

      /* Here the array is [2], because we have already inserted two items in the tests above*/
      expect(Array.isArray(responseBody.recipe_ingredients)).toBe(true);
      expect(responseBody.recipe_ingredients[2].ingredient_name).toBe(ingredientName);
      expect(responseBody.recipe_ingredients[2].ingredient_description).toBe(ingredientDescription);
      expect(responseBody.recipe_ingredients[2].ingredient_count).toBeUndefined();   
      expect(responseBody.recipe_ingredients[2].ingredient_unit).toBe(ingredientUnit);          

      done();
    });

    it("Try to add ingredient with missing Unit successfully", async (done) => {
      const response = await request(app)
        .post("/recipe/"+ String(recipeID)+ "/ingredients")
        .type("json")
        .send({
          ingredient_name: ingredientName,
          ingredient_description: ingredientDescription,
          ingredient_count: ingredientCount,      
          username: testUsername,
          password: testPassword
      });
             
      const responseBody = response.body;
      //console.log("INGREDIENTS: " + JSON.stringify(responseBody));
      expect(response.status).toBe(200);
      expect(responseBody._id).toBeDefined();
      expect(responseBody.username).toBe(testUsername);
      expect(responseBody.recipe_title).toBe(recipeTitle);
      expect(responseBody.recipe_description).toBe(recipeDescription);

      /* Here the array is [3], because we have already inserted two items in the tests above*/
      expect(Array.isArray(responseBody.recipe_ingredients)).toBe(true);
      expect(responseBody.recipe_ingredients[3].ingredient_name).toBe(ingredientName);
      expect(responseBody.recipe_ingredients[3].ingredient_description).toBe(ingredientDescription);
      expect(responseBody.recipe_ingredients[3].ingredient_count).toBe(ingredientCount);   
      expect(responseBody.recipe_ingredients[3].ingredient_unit).toBeUndefined();          

      done();
    });

    it("Try to add ingredient with missing ingredient name", async (done) => {
      const response = await request(app)
        .post("/recipe/"+ String(recipeID)+ "/ingredients")
        .type("json")
        .send({
          ingredient_description: ingredientDescription,
          ingredient_count: ingredientCount,
          ingredient_unit: ingredientUnit,         
          username: testUsername,
          password: testPassword
      });
             
      expect(response.status).toBe(500);       
      done();
    });

    it("Try to add ingredient with missing username", async (done) => {
      const response = await request(app)
        .post("/recipe/"+ String(recipeID)+ "/ingredients")
        .type("json")
        .send({
          ingredient_name: ingredientName,
          ingredient_description: ingredientDescription,
          ingredient_count: ingredientCount,
          ingredient_unit: ingredientUnit,                
          password: testPassword
      });
             
      expect(response.status).toBe(400);       
      done();
    });

    it("Try to add ingredient with missing password", async (done) => {
      const response = await request(app)
        .post("/recipe/"+ String(recipeID)+ "/ingredients")
        .type("json")
        .send({
          ingredient_name: ingredientName,
          ingredient_description: ingredientDescription,
          ingredient_count: ingredientCount,
          ingredient_unit: ingredientUnit,                
          username: testUsername
      });
             
      expect(response.status).toBe(400);       
      done();
    });

    it("Try to add ingredient with wrong password", async (done) => {
      const response = await request(app)
        .post("/recipe/"+ String(recipeID)+ "/ingredients")
        .type("json")
        .send({
          ingredient_name: ingredientName,
          ingredient_description: ingredientDescription,
          ingredient_count: ingredientCount,
          ingredient_unit: ingredientUnit,                
          username: testUsername,
          password: wrongPassword
      });
             
      expect(response.status).toBe(401);       
      done();
    });

    it("Read Recipe ingredients successfully", async (done) => {
      const response = await request(app).get("/recipe/"+ String(recipeID) + "/ingredients");
      
      expect(response.status).toBe(200);
      const responseBody = response.body;
      //console.log("INGREDIENTS: " + JSON.stringify(responseBody));

      /*Here we have ingredients of the tests above, they all have the same name */
      expect(responseBody[0].ingredient_name).toBe(ingredientName);
      expect(responseBody[1].ingredient_name).toBe(ingredientName);
      expect(responseBody[2].ingredient_name).toBe(ingredientName);
      expect(responseBody[3].ingredient_name).toBe(ingredientName);
      
      done();
    });
  });

/**************************************************************************************************/

  describe("Recipe Editing", () => {
    const newRecipeTitle = "Farofada do Cerrado";
    const newRecipeDescription = "A melhor de Goias!";

    const newIngredientName = "Feijao";
    const newIngredientDescription = "Feijao Preto";
    const newIngredientCount = 5000;
    const newIngredientUnit = "gramas";

    it("Edit ingredient successfully", async (done) => {
      /* Here we will test only for the first ingredient*/
      const response = await request(app)
        .put("/recipe/"+ String(recipeID)+ "/ingredients/0")
        .type("json")
        .send({
          ingredient_name: newIngredientName,
          ingredient_description: newIngredientDescription,
          ingredient_count: newIngredientCount,
          ingredient_unit: newIngredientUnit,         
          username: testUsername,
          password: testPassword
      });
             
      const responseBody = response.body;
      //console.log("INGREDIENTS: " + JSON.stringify(responseBody));
      expect(response.status).toBe(200);
      expect(responseBody._id).toBeDefined();
      expect(responseBody.username).toBe(testUsername);
      expect(responseBody.recipe_title).toBe(recipeTitle);
      expect(responseBody.recipe_description).toBe(recipeDescription);

      expect(Array.isArray(responseBody.recipe_ingredients)).toBe(true);
      expect(responseBody.recipe_ingredients[0].ingredient_name).toBe(newIngredientName);
      expect(responseBody.recipe_ingredients[0].ingredient_description).toBe(newIngredientDescription);
      expect(responseBody.recipe_ingredients[0].ingredient_count).toBe(newIngredientCount);   
      expect(responseBody.recipe_ingredients[0].ingredient_unit).toBe(newIngredientUnit);          

      done();
    });

    it("Delete Ingredient from Recipe sucessfully", async (done) => {
      /* Here we will test only for the first ingredient*/
      const response = await request(app)
      .delete("/recipe/" + String(recipeID) + "/ingredients/0")
      .type("json")
      .send({
        username: testUsername,
        password: testPassword
      });
      expect(response.status).toBe(200);
      done();
    });

    it("Edit Recipe Title successfully", async (done) => {     
      const response = await request(app)
        .put("/recipe/" + String(recipeID))
        .type("json")
        .send({
          username: testUsername,
          password: testPassword,
          recipe_title: newRecipeTitle,
          recipe_description: recipeDescription
      });
      
      expect(response.status).toBe(200);
      const responseBody = response.body;
      expect(responseBody._id).toBeDefined();
      expect(responseBody.username).toBe(testUsername);
      expect(responseBody.recipe_title).toBe(newRecipeTitle);
      expect(responseBody.recipe_description).toBe(recipeDescription);
      done();
    });

    it("Edit Recipe Description successfully", async (done) => {     
      const response = await request(app)
        .put("/recipe/" + String(recipeID))
        .type("json")
        .send({
          username: testUsername,
          password: testPassword,
          recipe_title: recipeTitle,
          recipe_description: newRecipeDescription
      });
      
      expect(response.status).toBe(200);
      const responseBody = response.body;
      expect(responseBody._id).toBeDefined();
      expect(responseBody.username).toBe(testUsername);
      expect(responseBody.recipe_title).toBe(recipeTitle);
      expect(responseBody.recipe_description).toBe(newRecipeDescription);
      done();
    });

    it("Edit Recipe Title AND Description successfully", async (done) => {     
      const response = await request(app)
        .put("/recipe/" + String(recipeID))
        .type("json")
        .send({
          username: testUsername,
          password: testPassword,
          recipe_title: newRecipeTitle,
          recipe_description: newRecipeDescription
      });
      
      expect(response.status).toBe(200);
      const responseBody = response.body;
      expect(responseBody._id).toBeDefined();
      expect(responseBody.username).toBe(testUsername);
      expect(responseBody.recipe_title).toBe(newRecipeTitle);
      expect(responseBody.recipe_description).toBe(newRecipeDescription);
      done();
    });

    it("Try to edit Recipe without providing username", async (done) => {
      const newRecipeTitle = "Farofada do Cerrado";

      const response = await request(app)
        .put("/recipe/" + String(recipeID))
        .type("json")
        .send({
          password: testPassword,
          recipe_title: newRecipeTitle,
          recipe_description: recipeDescription
      });
      
      expect(response.status).toBe(400);
      done();
    });

    it("Try to edit Recipe without providing password", async (done) => {
      const newRecipeTitle = "Farofada do Cerrado";

      const response = await request(app)
        .put("/recipe/" + String(recipeID))
        .type("json")
        .send({
          username: testUsername,
          recipe_title: newRecipeTitle,
          recipe_description: recipeDescription
      });
      
      expect(response.status).toBe(400);
      done();
    });

    it("Try to edit Recipe with wrong username", async (done) => {
      const newRecipeTitle = "Farofada do Cerrado";

      const response = await request(app)
        .put("/recipe/" + String(recipeID))
        .type("json")
        .send({
          username: wrongUsername,
          password: testPassword,
          recipe_title: newRecipeTitle,
          recipe_description: recipeDescription
      });
      
      expect(response.status).toBe(401);
      done();
    });

    it("Try to edit Recipe with wrong password", async (done) => {
      const newRecipeTitle = "Farofada do Cerrado";

      const response = await request(app)
        .put("/recipe/" + String(recipeID))
        .type("json")
        .send({
          username: testUsername,
          password: wrongPassword,
          recipe_title: newRecipeTitle,
          recipe_description: recipeDescription
      });
      
      expect(response.status).toBe(401);
      done();
    });
  });


  /**************************************************************************************************/

  describe("Recipe Deletion", () => {
    it("Try to delete Recipe without providing username", async (done) => {
      const response = await request(app)
        .delete("/recipe/" + String(recipeID))
        .type("json")
        .send({
          password: testPassword
      });
      expect(response.status).toBe(400);
      done();
    });

    it("Try to delete Recipe without providing password", async (done) => {
      const response = await request(app)
        .delete("/recipe/" + String(recipeID))
        .type("json")
        .send({
          username: testUsername
      });
      expect(response.status).toBe(400);
      done();
    });

    it("Try to delete Recipe with wrong password", async (done) => {
      const response = await request(app)
        .delete("/recipe/" + String(recipeID))
        .type("json")
        .send({
          username: testUsername,
          password: wrongPassword
      });
      expect(response.status).toBe(401);
      done();
    });

    it("Delete Recipe sucessfully", async (done) => {
      const response = await request(app)
      .delete("/recipe/" + String(recipeID))
      .type("json")
      .send({
        username: testUsername,
        password: testPassword
      });
      expect(response.status).toBe(200);
      done();
    });
  });
});

/*****************************************************************************************************/
/* Removed since backend list routes changed*/

// describe("List Management", () => {
//   it("Create List successfully", async (done) => {
//     const response = await request(app)
//       .post("/list")
//       .type("json")
//       .send({ username: testUsername, password: testPassword });

//     const responseBody = response.body;
//     //console.log("LISTA CRIADA: " + JSON.stringify(responseBody));
//     expect(response.status).toBe(200);
//     expect(responseBody.username).toBe(testUsername);
//     expect(Array.isArray(responseBody.items)).toBe(true);
//     expect(responseBody.items.length).toBe(0);

//     done();
//   });

//   it("Read List successfully", async (done) => {
//     const response = await request(app)
//       .post("/list/user")
//       .type("json")
//       .send({ username: testUsername, password: testPassword });

//     /* The API returns an array of JSON, therefore, we have to access the position [0] to retrieve it */  
//     const responseBody = response.body[0];
//     //console.log("LISTA CRIADA: " + JSON.stringify(responseBody));
//     expect(response.status).toBe(200);
//     expect(responseBody.username).toBe(testUsername);
//     expect(Array.isArray(responseBody.items)).toBe(true);

//     done();
//   });

//   it("Add item in List successfully", async (done) => {
//     const response = await request(app)
//       .post("/list/items")
//       .type("json")
//       .send({
//         name: itemName,
//         description: itemDescription,
//         count: itemCount,
//         done: false,         
//         username: testUsername,
//         password: testPassword
//     });
           
//     const responseBody = response.body;
//     //console.log("ITEMS: " + JSON.stringify(responseBody));
//     expect(response.status).toBe(200);
//     expect(responseBody._id).toBeDefined();
//     expect(responseBody.username).toBe(testUsername);

//     expect(Array.isArray(responseBody.items)).toBe(true);
//     expect(responseBody.items[0].name).toBe(itemName);
//     expect(responseBody.items[0].description).toBe(itemDescription);
//     expect(responseBody.items[0].count).toBe(itemCount);   
//     expect(responseBody.items[0].done).toBe(false);          

//     done();
//   });

//   it("Edit item in List successfully", async (done) => {
//     /*Here we will only test for the first item in List*/
//     const response = await request(app)
//       .put("/list/items/0")
//       .type("json")
//       .send({
//         name: itemName,
//         description: itemDescription,
//         count: itemCount,
//         done: true,         
//         username: testUsername,
//         password: testPassword
//     });
           
//     const responseBody = response.body;
//     //console.log("ITEMS: " + JSON.stringify(responseBody));
//     expect(response.status).toBe(200);
//     expect(responseBody._id).toBeDefined();
//     expect(responseBody.username).toBe(testUsername);

//     expect(Array.isArray(responseBody.items)).toBe(true);
//     expect(responseBody.items[0].name).toBe(itemName);
//     expect(responseBody.items[0].description).toBe(itemDescription);
//     expect(responseBody.items[0].count).toBe(itemCount);   
//     expect(responseBody.items[0].done).toBe(true);          

//     done();
//   });

//   it("Delete item from List sucessfully", async (done) => {
//     /*Here we will only test for the first item in List*/
//     const response = await request(app)
//     .delete("/list/items/0")
//     .type("json")
//     .send({
//       username: testUsername,
//       password: testPassword
//     });
//     expect(response.status).toBe(200);
//     const responseBody = response.body;
//     /* We inserted only one item, therefore, after removing it there shall be 0 items */
//     expect(responseBody.items.length).toBe(0);
//     done();
//   });

//   it("Delete List sucessfully", async (done) => {
//     const response = await request(app)
//     .delete("/list")
//     .type("json")
//     .send({
//       username: testUsername,
//       password: testPassword
//     });
//     expect(response.status).toBe(200);
//     done();
//   });
// });

/* Removed since backend list routes changed*/

/*****************************************************************************************************/

describe("User deletion", () => {
  it("Try to delete User without providing user password", async (done) => {
    const response = await request(app)
      .delete("/users")
      .type("json")
      .send({ 
        username: testUsername
      });
    expect(response.status).toBe(400);
    done();
  });

  it("Try to delete User without providing user name", async (done) => {
    const response = await request(app)
      .delete("/users")
      .type("json")
      .send({ 
        password: testPassword 
      });
    expect(response.status).toBe(400);
    done();
  });

  /*Only the user can delete his own account. One user can never delete others users accounts*/ 
  /*In this test we create a Second user and try to delete it when we are logged in the First 
  user account*/
  it("Try to delete User without authorization", async (done) => {
    /* Here we insert the second user directly on database */
    User.create({username: "SecondUser", password:"123456"});
    
    /*And here, we are logged in the first user acc and we trie to delete the acc of the second one*/ 
    const response = await request(app)
      .delete("/users")
      .type("json")
      .send({ 
        username: "SecondUser", 
        password: "123456"  
      });
    expect(response.status).toBe(401);
    done();
  });

  it("Delete User sucessfully", async (done) => {
    const response = await request(app)
      .delete("/users")
      .type("json")
      .send({ 
        username: testUsername, 
        password: testPassword 
      });
    expect(response.status).toBe(200);
    done();
  });
});