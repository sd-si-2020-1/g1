var express = require('express');
var path = require('path');
var logger = require('morgan');
var passport = require('passport');
var session = require('express-session');
var LocalStrategy = require('passport-local').Strategy;
var User = require('./models/User');

// Swagger docs setup
const swaggerUi = require("swagger-ui-express");
const swaggerDocument = require('./swagger.json');


// routers
var indexRouter = require('./routes/index');
var listRouter = require('./routes/list');
var recipeRouter = require('./routes/recipe');

// MongoDB
var db = require('./lib/database');
// MongoDB
db.init({
    "host": "mongo",
    "database": "appdb"
});

var app = express();

//app.use(cors());
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

// app.use(cookieParser());
app.use(session({
    name: 'session-id',
    secret: '123-456-789',
    saveUninitialized: false,
    resave: false
  }));

app.use(passport.initialize());
app.use(passport.session());
passport.use(new LocalStrategy(User.authenticate()));
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());

app.use(express.static(path.join(__dirname, 'public')));


//Docs route
app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocument));
console.log("Swagger docs at: http://localhost:3000/api-docs");
app.use('/', indexRouter);
app.use('/list', listRouter);
app.use('/recipe', recipeRouter);

module.exports = app;