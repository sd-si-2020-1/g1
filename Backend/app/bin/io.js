let socket_io = require('socket.io');
let io = socket_io();
let socketApi = {};

socketApi.io = io;

io.on('connection', (socket) => {
    console.log('user connected');
});

socketApi.sendNotification = function(recipe_title) {
    io.sockets.emit('notificacao', recipe_title);
};

module.exports = socketApi;

