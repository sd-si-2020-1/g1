const User = require('../../models/User');

let deleteUser = function(req, res, next){
    User.deleteOne({username: req.body.username})
    .then( result => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(result);   
    })
    .catch( err => next(err) );
}

module.exports = deleteUser

