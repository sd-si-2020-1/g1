let logoutMessage = 'You are successfully logged out!';
let notloggedinMessage = 'You are not logged in!'

let logout = function(req, res, next) {
    if (req.session) {
      req.logout();
      req.session.destroy((err) => {
        if (err) {
          console.log(err);
        } else {
          res.clearCookie('session-id');
          res.json({
            message: logoutMessage
          });
        }
      });
    } else {
      var err = new Error(notloggedinMessage);
      err.status = 403;
      next(err);
    }
  }

module.exports = logout