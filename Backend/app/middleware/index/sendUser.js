const User = require('../../models/User');

let sendUser = function(req, res, next) {
    User.findOne({
      username: req.body.username
    })
    .then( person => {
      res.statusCode = 200;
      res.setHeader('Content-Type', 'application/json');
      res.json(person);
    })
    .catch(err => {
        res.statusCode = 500;
        res.setHeader('Content-Type', 'application/json');
        next(err);
    });
  };

module.exports = sendUser