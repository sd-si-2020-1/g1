const User = require('../../models/User');
const passport = require('passport');

let signUpMessage = 'Registration Successful!';

let signUp = function(req, res, next) {

    User.register(
      new User({
        username: req.body.username
      }),
      req.body.password, (err, user) => {
        //console.log(user);
        if (err) {
          res.statusCode = 500;
          res.setHeader('Content-Type', 'application/json');
          res.json({
            err: err
          });
        } else {
          passport.authenticate('local')(req, res, () => {
            User.findOne({
              username: req.body.username
            }, (err, person) => {
              if (err){
                next(err);
              }
              res.statusCode = 200;
              res.setHeader('Content-Type', 'application/json');
              res.json({
                success: true,
                status: signUpMessage,
              });
            });
          })
        }
      })
  };

module.exports = signUp