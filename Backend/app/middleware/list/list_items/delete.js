const path = require('path');
const dirname_array = __dirname.split(path.sep);
const dirname = dirname_array[dirname_array.length-1]
const config = require('../../../config/middleware.json'); 
const model_path = config[dirname];
const Model = require(model_path);

module.exports = async function (req,res,next){
    const filter = {idLista: req.params.idLista};
    try {
        let response = await Model.findOne(filter);
        response.items.splice(req.params.pos, 1);
        let result = await response.save()
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(result);  
    }
    catch (err) {
        next(err);
    }
};