const path = require('path');
const dirname_array = __dirname.split(path.sep);
const dirname = dirname_array[dirname_array.length-1]
const config = require('../../config/middleware.json'); 
const model_path = config[dirname];
const Model = require(model_path);
var io = require('../../bin/io');

module.exports = function (req,res,next){
    Model.create(req.body)
    .then( result => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(result);
        reqNotificacao(result.recipe_title);
    })
    .catch( err => next(err) );
};

var reqNotificacao = (recipe_title) => {
    io.sendNotification(recipe_title);
};