const path = require('path');
const dirname_array = __dirname.split(path.sep);
const dirname = dirname_array[dirname_array.length-1]
const config = require('../../config/middleware.json'); 
const model_path = config[dirname];
const Model = require(model_path);

const assert = require('assert');

module.exports = async function (req,res,next){
    const filter = {username: req.body.username};
    try {
        let result = await Model.findOne(filter);
        assert.equal(result.username, req.body.username);
        next();
    }
    catch (err) {
        next(err);
    }
};