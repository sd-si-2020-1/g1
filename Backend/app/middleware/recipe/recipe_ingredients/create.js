const path = require('path');
const dirname_array = __dirname.split(path.sep);
const dirname = dirname_array[dirname_array.length-1]
const config = require('../../../config/middleware.json'); 
const model_path = config[dirname];
const Model = require(model_path);

module.exports = async function (req,res,next){
    const filter = { id: req.params.id };
    try {
        let response = await Model.findOne(filter);
        response.recipe_ingredients.push(req.body);
        let result = await response.save()
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(result);  
    }
    catch (err) {
        next(err);
    }
};