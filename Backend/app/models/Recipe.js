'use strict';

var mongoose        = require('mongoose'),
    Schema          = mongoose.Schema;
    
var AutoIncrement   = require('mongoose-sequence')(mongoose);
    
var RecipeIngredientsSchema = Schema({
    ingredient_name        : { type: String,   required: true },
    ingredient_description : { type: String,   required: false },
    ingredient_count       : { type: Number,   required: false },
    ingredient_unit        : { type: String,   required: false}
});

var RecipeSchema = Schema({
    recipe_title                : { type: String, required: true },
    username                    : { type: String, required: true },
    recipe_description          : { type: String, required: true },
    recipe_ingredients          : [RecipeIngredientsSchema]
});

RecipeSchema.plugin(AutoIncrement, {inc_field: 'id'});

var RecipeModel = mongoose.model('Recipe', RecipeSchema);
module.exports = RecipeModel;