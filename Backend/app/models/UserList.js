'use strict';

var mongoose        = require('mongoose'),
    Schema          = mongoose.Schema;

var AutoIncrement   = require('mongoose-sequence')(mongoose);

var UserListItemSchema = Schema({
    name        : { type: String,   required: true },
    description : { type: String,   required: false },
    count       : { type: Number,   default: 1 },
    done        : { type: Boolean,  default: false }
});

var UserListSchema = Schema({
    username                : { type: String, required: true },
    list_title              : { type: String, required: true },
    items                   : [UserListItemSchema]
});

UserListSchema.plugin(AutoIncrement, {inc_field: 'idLista'});

var UserListModel = mongoose.model('UserList', UserListSchema);
module.exports = UserListModel;