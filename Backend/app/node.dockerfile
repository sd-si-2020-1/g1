FROM node:14.13.1-buster

ENV NODE_ENV=development 
ENV PORT=3000

WORKDIR /usr/app
COPY package.json .
RUN npm install 
COPY . .
EXPOSE $PORT