const passport = require('passport');
const express = require('express');
const router = express.Router();

// middleware
const signUp = require('../middleware/index/signUp');
const sendUser = require('../middleware/index/sendUser');
const logout = require('../middleware/index/logout');
const deleteUser = require('../middleware/index/deleteUser');


router.get('/', (req,res) => res.sendStatus(200) );

router.post('/signup', signUp);

router.post('/login', passport.authenticate('local'), sendUser);

router.get('/logout', logout);

router.delete('/users', passport.authenticate('local'), deleteUser);

module.exports = router;