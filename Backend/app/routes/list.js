const passport = require('passport');
const express = require('express');
const router = express.Router();

// Middleware
const isListOwner = require('../middleware/list/isOwner');
const createList = require('../middleware/list/create');
const readList = require('../middleware/list/read');
const updateList = require('../middleware/list/update');
const deleteList =  require('../middleware/list/delete');

const createListItem = require('../middleware/list/list_items/create');
const readListItems = require('../middleware/list/list_items/read');
const updateListItem = require('../middleware/list/list_items/update');
const deleteListItem = require('../middleware/list/list_items/delete');

router.post('/', passport.authenticate('local'), createList);
router.post('/user', passport.authenticate('local'), isListOwner, readList);
router.put('/', passport.authenticate('local'), isListOwner, updateList);
router.delete('/', passport.authenticate('local'), deleteList);

router.post('/:idLista/items', passport.authenticate('local'), isListOwner, createListItem );
//Essa rota de baixo é redundante com get/ readList
router.get('/:idLista/items', passport.authenticate('local'), isListOwner, readListItems );
router.put('/:idLista/items/:pos', passport.authenticate('local'), isListOwner, updateListItem );
router.delete('/:idLista/items/:pos', passport.authenticate('local'), isListOwner, deleteListItem );

module.exports = router;
