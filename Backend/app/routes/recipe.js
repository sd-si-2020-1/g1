const passport = require('passport');
const express = require('express');
const router = express.Router();

// Middleware

const isRecipeOwner = require('../middleware/recipe/isOwner');
const isOwnerList = require('../middleware/recipe/isOwnerList');

const createRecipe = require('../middleware/recipe/create');
const updateRecipe = require('../middleware/recipe/update');
const deleteRecipe = require('../middleware/recipe/delete');
const readRecipe = require('../middleware/recipe/read');
const readRecipeById = require('../middleware/recipe/readById');
const readByUsername = require('../middleware/recipe/readByUsername');
const createRecipeIngredient = require('../middleware/recipe/recipe_ingredients/create');
const deleteRecipeIngredient = require('../middleware/recipe/recipe_ingredients/delete');
const updateRecipeIngredient = require('../middleware/recipe/recipe_ingredients/update');
const readRecipeIngredients = require('../middleware/recipe/recipe_ingredients/read');

// Receitas
router.post('/', passport.authenticate('local'), createRecipe);
router.get('/', readRecipe);
router.post('/user', passport.authenticate('local'), isOwnerList, readByUsername);
router.get('/:id', readRecipeById);
router.put('/:id', passport.authenticate('local'), isRecipeOwner, updateRecipe);
router.delete('/:id', passport.authenticate('local'), isRecipeOwner, deleteRecipe);

// Ingredientes das receitas
router.post('/:id/ingredients', passport.authenticate('local'), isRecipeOwner, createRecipeIngredient );
router.get('/:id/ingredients', readRecipeIngredients );
router.put('/:id/ingredients/:pos', passport.authenticate('local'), isRecipeOwner, updateRecipeIngredient );
router.delete('/:id/ingredients/:pos', passport.authenticate('local'), isRecipeOwner, deleteRecipeIngredient );

module.exports = router;