# Sobre

Este documento especifica os detalhes do **frontend**.

# Sumário
- [Cronograma](#Cronograma)
- [Instalação e Execução](#Instalação-e-Execução)
- [Contato](#Contato)

# Cronograma

- 17/11/20  - Entrega do primeiro Sprint
- 23/11/20 - Entrega do segundo Sprint
- 30/10/20 - Recesso
- 06/01/21 - Entrega do terceiro Sprint
- 14/01/21 - Entrega do quarto 
- 21/01/21 - Entrega do quinto e último Sprint (final)
- 22/01/21 - Entrega de apresentação individual: sua auto-avaliação (texto) e apresentação ao vivo ou participação em vídeo em 15 minutos (demonstrativo,  slides são opcionais)


# Instalação e Execução
Siga os passos abaixo para executar a aplicação em seu smartphone físico.

```
git clone https://gitlab.com/sd-si-2020-1/g1.git

cd g1/Frontend/ReceitasApp

npm install
```

Antes de inicializar o frontend, certifique-se de colocar o seu ip na url base do axios em [Frontend/ReceitasApp/src/services/api.js](Frontend/ReceitasApp/src/services/api.js). A porta usada deve ser a 3000, ou seja, no final a estrutura será **http://{seu ip}:3000**

![](https://media.discordapp.net/attachments/802308639083462716/802310267383054336/unknown.png)


```
expo start
```

Após a execução do comando expo start, será mostrado um QR code no terminal e também em uma janela de seu navegador padrão. 

![alt text](https://miro.medium.com/max/3820/1*N7AWqIVrSJtDjiPS4se4pg.png)

Faça a leitura do QR code com o aplicativo do Expo client disponível na [**AppStore**](https://apps.apple.com/br/app/expo-client/id982107779) ou na [**Playstore**](https://play.google.com/store/apps/details?id=host.exp.exponent&hl=pt_BR&gl=US).

Feito a leitura, o nosso aplicativo será executado em seu smartphone através do Expo client. 

Se você não tiver um smartphone físico é possível executar a aplicação em um emulador. Para isso verifique as instruções na [documentação oficial do Expo](https://docs.expo.io/workflow/android-studio-emulator/). 

# Contato

Qualquer dúvida ou sugestão, entre em contado com os [integrantes do projeto](#Integrantes-e-suas-responsabilidades), ou abra uma nova [issue](https://gitlab.com/sd-si-2020-1/g1/-/issues). 
