import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View
} from 'react-native';
import {
    Area,
    Description,
    Ingredient,
    IngredientDescription,
    IngredientName,
    List,
    Name,
    Recipe,
    RecipeButton,
    RecipeButtonText
} from '../pages/Main/styles';
import Icon from 'react-native-vector-icons/FontAwesome';
import {Drawer, Container, Header, Content,Button } from 'native-base';
export class SideBar extends Component {


    render(){

        return (
            <View style={[ styles.container, { backgroundColor: '#fff' } ]}>
                <Text>
                    <Icon name="rocket" size={30} color="#900" />
                    Conteúdo side bar
                </Text>
            </View>
        );
    }
};

export default class App extends Component<> {
    closeDrawer = () => {
        this.drawer._root.close()
    };
    openDrawer = () => {
        this.drawer._root.open()
    };
    render() {
        return (
            <Drawer
                style={styles.safeArea}
                ref={(ref) => { this.drawer = ref; }}
                content={<SideBar navigator={this.navigator} />}
                onClose={() => this.closeDrawer()}>
                <Container>
                    <Header>
                        <Container style={{flexDirection: 'row'}}>
                            <Icon onPress={() => this.openDrawer()} name="bars" size={30} color="#000" />
                        </Container>
                    </Header>
                </Container>
            </Drawer>

        );
    }
}
const styles = StyleSheet.create({
    safeArea: {
        flex: 1,
        margin: 0,
        padding: 0,
        color: '#000',
        backgroundColor: '#000'
    },
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#565656',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
});