import React, {Component} from 'react';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import {ContainerLogo, Logo} from '../pages/Login/styles';
import {Platform} from 'react-native';
import { Dimensions } from 'react-native';

export default class LogoLogin extends Component {
    render() {

        return (
                <ContainerLogo>
                    <Logo/>
                </ContainerLogo>
        );
    }

}
