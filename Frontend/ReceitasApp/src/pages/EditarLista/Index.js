import React, {Component} from 'react';
import {AsyncStorage, ScrollView} from 'react-native';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import {Ingredients, IngredientsInputs, Input, InputIngredients, Name, SubmitButton} from './styles';

import {Container, ContainerRow, List, Title} from '../Receita/styles'
import Icon from "react-native-vector-icons/AntDesign";
import api from "../../services/api"

Icon.loadFont();

export default class EditarLista extends Component {
    static navigationOptions = {
        title: 'EditarLista',
    };

    static initialState = {};

    state = {
        list_title: "",
        ingredients: [],
        name: "",
        description: "",
        username: "",
        password: "",
        idLista: ""
    };

    excluirIngrediente = (name) => {
        const {ingredients} = this.state;
        let data = ingredients.filter(item => item.name !== name);
        this.setState({
            ingredients: data
        })
    }

    addIngredient = (data) => {
        const {ingredients} = this.state;

        this.setState(
            {
                ingredients: [...ingredients, data]
            }
        );
    };

    async componentDidMount() {
        const list = this.props.route.params.list;

        const storage = await AsyncStorage.getItem('login');
        let data = JSON.parse(storage);

        if (!data.username || !data.password){
            const {navigation} = this.props;
            navigation.navigate('Home');
        }
        this.setState({
            username: data.username,
            password: data.password,
            list_title: list.list_title,
            idLista: list.idLista,
            ingredients: list.items
        })
    }

    componentDidUpdate(_, prevState) {
    }

    handleNavigate = () => {
        const {navigation} = this.props;
        navigation.navigate('Home');
    };

    async criarLista(){
        const {username, password, list_title} = this.state;

        const list = {
            username,
            password,
            list_title
        };
        let data = this.encodeObject(list);
        this.postList(data);
    }

    async postList(data) {
        const response = await api.put(`/list`, data, {'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'}).then(
            (res) => {
                try {
                    this.setState({idLista: res.data.idLista})
                    this.createIngredients()
                } catch (e) {
                    this.setState({messageError: 'Username ou Password incorretos.'})
                    this.showAlert();
                }
            });
    }

    createIngredients() {
        const { idLista, username, password, ingredients} = this.state;

        ingredients.forEach(item => {
            const ingredient = {
                username,
                password,
                name: item.name,
               description: item.description,
            };
            let data = this.encodeObject(ingredient);

            let postStatus = this.postIngredient(data, idLista);
            if (postStatus){
            }
        });
        this.handleNavigate();
    }

    async postIngredient(data, id) {
        const response = await api.post(`/list/` + id + '/items', data, {'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'}).then(
            (res) => {
                try {

                } catch (e) {
                    this.setState({messageError: 'Sistema temporariamente Indisponível'})
                    this.showAlert();
                }
            });
        return !!response.data;
    }

    encodeObject = (object) => {
        var data = [];
        for (var property in object) {
            var encodedKey = encodeURIComponent(property);
            var encodedValue = encodeURIComponent(object[property]);
            data.push(encodedKey + "=" + encodedValue);
        }

        data = data.join("&");
        return data;
    };

    render() {
        const {
            recipe_title,
            ingredients,
        } = this.state;

        const dataInicial = {
            name: "",
            description: "",
        };

        let data = dataInicial;

        const {navigation} = this.props;
        const list = this.props.route.params.list;

        return (
            <SafeAreaProvider style={{backgroundColor: '#fff'}}>
                <ScrollView>
                    <ContainerRow>
                        <Input
                            autoCorrect={false}
                            autoCapitalize="none"
                            placeholder="Título Lista"
                            defaultValue={list.list_title}
                            onChangeText={text => this.setState({list_title: text})}
                        />
                    </ContainerRow>
                    <ContainerRow>
                        <Title>
                            Ingredientes
                        </Title>
                    </ContainerRow>
                    <Container>
                        <IngredientsInputs>
                            <InputIngredients
                                autoCorrect={false}
                                autoCapitalize="none"
                                placeholder="Nome"
                                onChangeText={text => data.name = text}
                            />
                            <InputIngredients
                                autoCorrect={false}
                                autoCapitalize="none"
                                placeholder="Descrição"
                                onChangeText={text => data.description = text}
                            />
                        </IngredientsInputs>
                        <SubmitButton onPress={() => this.addIngredient(data)}>
                            <Name>Adicionar ingrediente</Name>
                        </SubmitButton>
                        <List
                            data={ingredients}
                            keyExtractor={(ingredient, index) => ingredient.index}
                            renderItem={({item}) => (
                                <Ingredients>
                                    <InputIngredients
                                        autoCorrect={false}
                                        autoCapitalize="none"
                                        placeholder="Nome"
                                        value={item.name}
                                    />
                                    <InputIngredients
                                        autoCorrect={false}
                                        autoCapitalize="none"
                                        placeholder="Descrição"
                                        value={item.description}
                                    />
                                    <SubmitButton onPress={() => this.excluirIngrediente(item.name)}>

                                    </SubmitButton>
                                </Ingredients>
                            )}
                        />
                    </Container>
                    <Container style={{alignItems: 'center'}}>
                        <SubmitButton onPress={() => this.criarLista()}>
                            <Name>Salvar</Name>
                        </SubmitButton>
                    </Container>
                </ScrollView>
            </SafeAreaProvider>
        );
    }

}
