import styled from "styled-components/native";
import {RectButton} from "react-native-gesture-handler";
import {Dimensions} from 'react-native';


export const ImageProfile = styled.Image.attrs({
    resizeMode: 'cover'
})`
borderRadius: 1000px;
height: 160px;
width: 160px;
`;

export const ContainerImage = styled.View`
flex-direction: column;
justify-content: center;
align-items: center;
background: #fff;
`;
export const ContainerLogin = styled.View`
flex: 1;
flex-direction: column;
padding: 0 20px;
background: #fff;
`;

export const Form = styled.View`
flex: 1;
flex-direction: column;
border-bottom-width: 1px;
border-color: #eee
`;


export const ContainerPreparo = styled.View`
flex: 1;
flex-direction: column;
padding: 0 20px;
background: #E4D5D5, 100%;
margin: 10px 20px;
`;

export const Input = styled.TextInput.attrs({
    placeholderTextColor: '#535353',
})`
font-size: 22px;
flex: 1;
margin: 20px;
justify-content: center;
align-items: center;
color: #434343;
font-weight: bold;
borderBottomWidth: 1px;
`;

export const InputIngredients = styled.TextInput.attrs({
    placeholderTextColor: '#535353',
})`
font-size: 15px;
justify-content: center;
width: ${Dimensions.get('window').width/2.5}px;
align-items: center;
color: #434343;
font-weight: bold;
background: #fff;
borderBottomWidth: 1px;
`;

export const SubmitButton = styled(RectButton)`
justify-content: center;
align-items: center;
height: 40px;
background: #FCCA17;
border-radius: 4px;
padding: 0 12px;
`;

export const Recipe = styled.TouchableOpacity`
flex-direction: row;
height: ${Dimensions.get('window').width / 9}px;
width: ${Dimensions.get('window').width}px;
margin-top: 15px;
margin-right: 1px;
margin-left: 1px;
flex: 1;
`;

export const Ingredients = styled.View`
flex-direction: row;
justify-content: space-between;
height: ${Dimensions.get('window').width / 9}px;
margin-top: 15px;
margin-right: 1px;
margin-left: 1px;
flex: 1;
`;

export const IngredientsInputs = styled.View`
flex-direction: row;
justify-content: space-between;
margin-bottom: 15px;
margin-right: 1px;
margin-left: 1px;
flex: 1;
`;

export const Name = styled.Text`
font-size: 16px;
color: #565656;
font-weight: bold;
margin-top: 4px;
text-align: center;
`;

export const Description = styled.TextInput.attrs({
    placeholderTextColor: '#535353',
    multiline: true
})`
font-size: 15px;
line-height: 18px;
color: #565656;
margin-top: 5px;
text-align: left;
`;