import React, {Component} from 'react';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import {Container, Description, List, Name, Recipe} from './styles';
import {Platform} from 'react-native';
import {AsyncStorage} from "react-native";

export default class Favoritos extends Component {
    static navigationOptions = {
        title: 'Favoritos',
    };

    static initialState = {};

    state = {
        recipes: null,
    };

    async componentDidMount() {
        const {navigation} = this.props;
        const recipeList = await AsyncStorage.getItem('favoritos');
        let recipeListJson = JSON.parse(recipeList);
        this.setState({recipes: recipeListJson})
    }

    async componentDidUpdate(_, prevState) {
        const {navigation} = this.props;
        const recipeList = await AsyncStorage.getItem('favoritos');
        let recipeListJson = JSON.parse(recipeList);
        this.setState({recipes: recipeListJson})
    }

    // handleNavigate = (recipe) => {
    //     const {navigation} = this.props;
    //     navigation.navigate('User', {recipe});
    // };

    handleNavigateReceita = (item) => {
        const {navigation} = this.props;
        navigation.navigate('Receita', { recipe: item });
    };

    render() {
        const {recipes} = this.state;

        return (
            <SafeAreaProvider
                style={{flex: 1, paddingTop: Platform.OS === 'android' ? 25 : 0, backgroundColor: '#FCCA17'}}>
                    <Container>
                        <List
                            data={recipes}
                            keyExtractor={recipe => recipe._id}
                            renderItem={({item}) => (
                                <Recipe onPress={() => this.handleNavigateReceita(item)}>
                                    <Name>{item.recipe_title}</Name>
                                    <Description>{item.recipe_description}</Description>
                                </Recipe>
                            )}
                        />

                    </Container>
            </SafeAreaProvider>
        );
    }

}
