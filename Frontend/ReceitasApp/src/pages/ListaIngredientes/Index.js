import React, {Component} from 'react';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import {
    Container,
    ContainerPreparo,
    ContainerRow,
    Description,
    List,
    Name,
    Recipe,
    SubmitButton,
    Title
} from './styles';
import {AsyncStorage} from "react-native";

export default class ListaIngredientes extends Component {
    static navigationOptions = {
        title: 'ListaIngredientes',
    };

    static initialState = {};

    state = {
        recipe: {},
    };

    async componentDidMount() {
        const recipe = this.props.route.params.recipe;
        let storage = await AsyncStorage.getItem('historico');

        storage = storage ? JSON.parse(storage) : [];
        // storage = [];
        if (storage.length > 9){
            storage.slice(1,storage.length);
        }
        storage.push(recipe);
        await AsyncStorage.setItem('historico', JSON.stringify(storage));

    }

    async componentDidUpdate(_, prevState) {
        const recipe = this.props.route.params.recipe;
        let storage = await AsyncStorage.getItem('historico');

        storage = storage ? JSON.parse(storage) : [];
        // storage = [];
        if (storage.length > 9){
            storage.slice(1,storage.length);
        }
        storage.push(recipe);
        await AsyncStorage.setItem('historico', JSON.stringify(storage));
    }

    handleNavigate = () => {
        const {navigation} = this.props;
        navigation.navigate('Home');
    };

    async salvarReceita (item) {
        // const {navigation} = this.props;
        // let storage = await AsyncStorage.getItem('favoritos');
        //
        // storage = storage ? JSON.parse(storage) : [];
        // storage.push(item);
        // await AsyncStorage.setItem('favoritos', JSON.stringify(storage));
        // navigation.navigate('Home');
    }

    render() {
        const list = this.props.route.params.list;
        return (
            <SafeAreaProvider style={{backgroundColor: '#fff'}}>
                <ContainerRow>
                    <Title>
                        {list.username}
                    </Title>
                </ContainerRow>
                <ContainerRow>
                    <Title>
                        Ingredientes
                    </Title>
                </ContainerRow>
                <Container>
                    <List
                        data={list.items}
                        keyExtractor={ingredient => ingredient._id}
                        renderItem={({item}) => (
                            <Recipe onPress={() => this.handleNavigateReceita()}>
                                <Name>{item.ingredient_count} {item.ingredient_unit} - {item.ingredient_name}</Name>
                            </Recipe>
                        )}
                    />
                </Container>

                <Container style={{alignItems: 'center'}}>
                    <SubmitButton onPress={() => this.salvarReceita()}>
                        <Name>Salvar</Name>
                    </SubmitButton>
                </Container>
            </SafeAreaProvider>
        );
    }

}
