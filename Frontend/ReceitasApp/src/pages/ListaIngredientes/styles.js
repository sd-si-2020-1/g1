import styled from "styled-components/native";
import {RectButton} from "react-native-gesture-handler";
import {Dimensions} from 'react-native';


export const ImageProfile = styled.Image.attrs({
    resizeMode: 'cover'
})`
borderRadius: 1000px;
height: 160px;
width: 160px;
`;

export const List = styled.FlatList.attrs({
    showsVerticalScrollIndicator: false,
})`
margin-top: 10px;
`;

export const ContainerImage = styled.View`
flex-direction: column;
justify-content: center;
align-items: center;
background: #fff;
`;
export const Container = styled.View`
flex-direction: column;
padding: 0 20px;
background: #fff;
margin-bottom: 20px;
`;

export const ContainerPreparo = styled.View`
flex: 0.6;
flex-direction: column;
padding: 0 20px;
background: #E4D5D5, 100%;
margin: 10px 20px;
`;
export const SubmitButton = styled(RectButton)`
justify-content: center;
align-items: center;
height: 40px;
width: 120px;
background: #FCCA17;
border-radius: 20px;
`;

export const ContainerRow = styled.View`
flex-direction: row;
align-items: center;
justify-content: space-between;
background: #fff;
`;
export const LogoutButton = styled(RectButton)`
justify-content: center;
align-items: center;
height: 40px;
width: 80px;
margin: 0 20px;
background: #FCCA17;
border-radius: 4px;
padding: 0 12px;
`;
export const Title = styled.Text`
font-size: 22px;
width: ${Dimensions.get('window').width};
margin: 20px;
justify-content: center;
align-items: center;
color: #434343;
font-weight: bold;
`;

export const Recipe = styled.TouchableOpacity`
height: ${Dimensions.get('window').width / 10}px;
flex-direction: row;
justify-content: space-between;
padding: 0 10px;
margin-top: 15px;
border-radius: 2px;
border: 1px;
`;

export const Name = styled.Text`
font-size: 16px;
color: #565656;
font-weight: bold;
margin-top: 4px;
text-align: center;
`;

export const PlaceHolder = styled.Text`
font-size: 16px;
color: #565656;
font-weight: bold;
margin-top: 4px;
`;

export const IngredientName = styled.Text`
font-size: 14px;
color: #565656;
font-weight: bold;
margin-top: 4px;
text-align: center;
`;

export const Description = styled.Text`
font-size: 15px;
line-height: 18px;
color: #000000;
margin-top: 5px;
`;

export const IngredientDescription = styled.Text.attrs({
    numberOfLines: 2
})`
font-size: 13px;
line-height: 18px;
color: #565656;
margin-top: 5px;
text-align: center;
`;

export const RecipeButton = styled(RectButton)`
margin-top: 10px;
margin-bottom: 10px;
align-self: stretch;
border-radius: 4px;
background: transparent;
justify-content: center;
align-items: center;
height: 36px;
`;

export const SomarZerarButton = styled(RectButton)`
margin-top: 10px;
align-self: stretch;
border-radius: 4px;
background: #f48024;
justify-content: center;
align-items: center;
height: 36px;
`;

export const RecipeButtonText = styled.Text`
font-size: 14px;
font-weight: bold;
color: #000000;
text-transform: uppercase;
`;
