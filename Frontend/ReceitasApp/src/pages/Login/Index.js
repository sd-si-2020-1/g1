import React, {Component} from 'react';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import {ContainerLogin, Form, Input, Name, PlaceHolder, SubmitButton} from './styles';
import {StackActions} from '@react-navigation/native';
import {ScrollView} from "react-native";
import api from "../../services/api"
import {AsyncStorage} from 'react-native';
import AwesomeAlert from 'react-native-awesome-alerts';


export default class Main extends Component {
    static navigationOptions = {
        title: 'Receitas',
    };

    static initialState = {};

    state = {
        user: '',
        password: '',
        showAlert: false,
        messageError: ""
    };

    showAlert = () => {
        this.setState({
            showAlert: true
        });
    };

    hideAlert = () => {
        this.setState({
            showAlert: false
        });
    };

    async componentDidMount() {
        const login = await AsyncStorage.getItem('login');
        let data = JSON.parse(login);
        this.setState({
            user: data.username,
        })
    }

    componentDidUpdate(_, prevState) {

    }

    handleNavigate = () => {
        const {navigation} = this.props;
        navigation.dispatch(
            StackActions.replace('Home')
        );
    };

    login() {
        const {user, password} = this.state;
        var details = {
            'username': user,
            'password': password,
        };

        var data = [];
        for (var property in details) {
            var encodedKey = encodeURIComponent(property);
            var encodedValue = encodeURIComponent(details[property]);
            data.push(encodedKey + "=" + encodedValue);
        }

        data = data.join("&");

        this.postLogin(data);
    }

    async postLogin(data) {
        const {navigation} = this.props;
        const {password} = this.state;
        const response = await api.post(`/login`, data, {'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'}).then(
            (res) => {
                    try {
                        let storage = res.data;
                        storage.password = password;
                        AsyncStorage.setItem('login', JSON.stringify(storage));
                        navigation.dispatch(
                            StackActions.replace('Home')
                        );
                    } catch (e) {
                    }
        })
            .catch((error) => {
                this.setState({messageError: 'Username ou Password incorretos.'})
                this.showAlert();
            });
    }

    render() {
        const {user, password, showAlert, messageError} = this.state;
        const {navigation} = this.props;

        return (
            <SafeAreaProvider>
                <ScrollView>
                    <ContainerLogin>
                        <Form>
                            <PlaceHolder>Usuário</PlaceHolder>
                            <Input
                                autoCorrect={false}
                                autoCapitalize="none"
                                placeholder=""
                                value={user}
                                onChangeText={text => this.setState({user: text})}
                            />
                            <PlaceHolder>Senha</PlaceHolder>
                            <Input
                                autoCorrect={false}
                                autoCapitalize="none"
                                placeholder=""
                                value={password}
                                onChangeText={text => this.setState({password: text})}
                            />
                            <SubmitButton onPress={() => this.login()}>
                                <Name>Login</Name>
                            </SubmitButton>
                        </Form>
                    </ContainerLogin>
                </ScrollView>
                <AwesomeAlert
                    show={showAlert}
                    showProgress={false}
                    title="Error"
                    message={messageError}
                    closeOnTouchOutside={true}
                    closeOnHardwareBackPress={false}
                    showCancelButton={false}
                    showConfirmButton={true}
                    confirmText="Entendi"
                    confirmButtonColor="#FCCA17"
                    onCancelPressed={() => {
                        this.hideAlert();
                    }}
                    onConfirmPressed={() => {
                        this.hideAlert();
                    }}
                />
            </SafeAreaProvider>
        );
    }

}