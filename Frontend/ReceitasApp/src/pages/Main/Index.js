import React, {Component} from 'react';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import {
    Container,
    ContainerHamburguer,
    Description,
    List,
    Name,
    Recipe,
    RecipeButton,
    RecipeButtonText
} from './styles';
import {Platform, StyleSheet, View, RefreshControl} from 'react-native';
import {Drawer} from 'native-base';
import Icon from "react-native-vector-icons/FontAwesome";
import api from "../../services/api"

export default class Main extends Component {
    static navigationOptions = {
        title: 'Main',
    };

    static initialState = {};

    state = {
        recipe: [],
        refreshing: false,
    };

    async componentDidMount() {
        const {recipe} = this.state;

        const response = await api.get(`/recipe`);
        var data = response.data;

        this.setState({
            recipe: data,
        })
    }

    _onRefresh() {
        this.setState({refreshing: true});
        this.componentDidMount().then(()=>{this.setState({refreshing: false})});
    }

    async componentDidUpdate(_, prevState) {

    }

    // handleNavigate = (recipe) => {
    //     const {navigation} = this.props;
    //     navigation.navigate('User', {recipe});
    // };

    handleNavigateReceita = (item) => {
        const {navigation} = this.props;
        navigation.navigate('Receita', {recipe: item});
    };

    closeDrawer = () => {
        this.drawer._root.close()
    };
    openDrawer = () => {
        this.drawer._root.open()
    };

    render() {
        const {recipe} = this.state;
        const {navigation} = this.props;
        console.log(recipe);

        return (
            <SafeAreaProvider
                style={{flex: 1, paddingTop: Platform.OS === 'android' ? 25 : 0, backgroundColor: '#FCCA17'}}>
                <Drawer
                    style={styles.safeArea}
                    ref={(ref) => {
                        this.drawer = ref;
                    }}
                    content={<SideBar navigation={navigation}/>}
                    onClose={() => this.closeDrawer()}>
                    <ContainerHamburguer>
                        <Icon onPress={() => this.openDrawer()} name="bars" size={40} color="#000"/>
                    </ContainerHamburguer>
                    <Container>
                        <List
                            data={recipe}
                            keyExtractor={recipe => recipe._id}
                            refreshControl={
                                <RefreshControl
                                    refreshing={this.state.refreshing}
                                    onRefresh={this._onRefresh.bind(this)}
                                />}
                            renderItem={({item}) => (
                                <Recipe onPress={() => this.handleNavigateReceita(item)}>
                                    <Name>{item.recipe_title}</Name>
                                    <Description>{item.recipe_description}</Description>
                                </Recipe>
                            )}
                        />

                    </Container>
                </Drawer>
            </SafeAreaProvider>
        );
    }

}

const SideBar = ({navigation}) =>
    <View style={[styles.container]}>
        <RecipeButton onPress={() => navigation.navigate('MinhasListas')}>
            <RecipeButtonText>Minhas Listas</RecipeButtonText>
        </RecipeButton>
        <RecipeButton onPress={() => navigation.navigate('MinhasReceitas')}>
            <RecipeButtonText>Minhas Receitas</RecipeButtonText>
        </RecipeButton>
    </View>;

const styles = StyleSheet.create({
    safeArea: {
        flex: 1,
        margin: 0,
        padding: 0,
        maxHeight: 10,
    },
    container: {
        flex: 1,
        backgroundColor: '#FCCA17'
    },
});