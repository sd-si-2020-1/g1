import React, {Component} from 'react';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import {Container, List, Name, Recipe} from "../MinhasReceitas/styles";
import Icon from "react-native-vector-icons/AntDesign";
import {SubmitButton} from "../Receita/styles";
import {AsyncStorage} from "react-native";
import api from "../../services/api"
import AwesomeAlert from "react-native-awesome-alerts";


Icon.loadFont();

export default class MinhasListas extends Component {
    static navigationOptions = {
        title: 'MinhasListas',
    };

    static initialState = {};

    state = {
        recipes: [],
        user: '',
        password: '',
        showAlert: false,
        messageError: ""
    };

    showAlert = () => {
        this.setState({
            showAlert: true
        });
    };

    hideAlert = () => {
        this.setState({
            showAlert: false
        });
    };

    async componentDidMount() {

        const login = await AsyncStorage.getItem('login');
        let storage = JSON.parse(login);
        var details = {
            'username': storage.username,
            'password': storage.password,
        };

        let body = this.encodeObject(details);

        await api.post(`/list/user`, body, {'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'}).then(
            (res) => {
                var data = res.data;
                this.setState({
                    recipes: data,
                })
            }).catch((error) => {
        });

    }

    encodeObject(object) {
        var data = [];
        for (var property in object) {
            var encodedKey = encodeURIComponent(property);
            var encodedValue = encodeURIComponent(object[property]);
            data.push(encodedKey + "=" + encodedValue);
        }

        data = data.join("&");
        return data;
    }

    async componentDidUpdate(_, prevState) {
    }

    handleNavigate = (item) => {
        const {navigation} = this.props;
        navigation.navigate('EditarLista', {list: item});
    };

    async novaLista() {
        const {navigation} = this.props;
        navigation.navigate('NovaLista');
    };

    render() {
        const {recipes, showAlert, messageError} = this.state;
        const {navigation} = this.props;

        return (
            <SafeAreaProvider>
                <Container>
                    <List
                        data={recipes}
                        keyExtractor={recipe => recipe._id}
                        renderItem={({item}) => (
                            <Recipe onPress={() => this.handleNavigate(item)}>
                                <Name>{item.list_title}</Name>
                                <Name>{item.items.length}</Name>
                                <Icon name="arrowright" size={25} color="#585858"/>
                            </Recipe>
                        )}
                    />
                </Container>
                <Container style={{alignItems: 'center'}}>
                    <SubmitButton onPress={() => this.novaLista()}>
                        <Name>Nova Lista</Name>
                    </SubmitButton>
                </Container>
                <AwesomeAlert
                    show={showAlert}
                    showProgress={false}
                    title="Error"
                    message={messageError}
                    closeOnTouchOutside={true}
                    closeOnHardwareBackPress={false}
                    showCancelButton={false}
                    showConfirmButton={true}
                    confirmText="Entendi"
                    confirmButtonColor="#FCCA17"
                    onCancelPressed={() => {
                        this.hideAlert();
                    }}
                    onConfirmPressed={() => {
                        this.hideAlert();
                    }}
                />
            </SafeAreaProvider>
        );
    }

}
