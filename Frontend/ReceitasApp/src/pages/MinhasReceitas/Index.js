import React, {Component} from 'react';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import {Container, List, Name, Recipe} from "../MinhasReceitas/styles";
import Icon from "react-native-vector-icons/AntDesign";
import {SubmitButton} from "../Receita/styles";
import {AsyncStorage} from "react-native";
import api from "../../services/api";

Icon.loadFont();

export default class MinhasReceitas extends Component {
    static navigationOptions = {
        title: 'MinhasReceitas',
    };

    static initialState = {};

    state = {
        recipes: []
    };

    async componentDidMount() {
        const {recipe} = this.state;

        const login = await AsyncStorage.getItem('login');
        let storage = JSON.parse(login);
        var details = {
            'username': storage.username,
            'password': storage.password,
        };

        let body = this.encodeObject(details);

        await api.post(`/recipe/user`, body, {'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'}).then(
            (res) => {
                var data = res.data;
                this.setState({
                    recipes: data,
                })
            }).catch((error) => {

        });

    }

    encodeObject(object) {
        var data = [];
        for (var property in object) {
            var encodedKey = encodeURIComponent(property);
            var encodedValue = encodeURIComponent(object[property]);
            data.push(encodedKey + "=" + encodedValue);
        }

        data = data.join("&");
        return data;
    }


    async componentDidUpdate(_, prevState) {
    }

    handleNavigateReceita = (item) => {
        const {navigation} = this.props;
        navigation.navigate('Receita', { recipe: item });
    };

    NovaReceita = () => {
        const {navigation} = this.props;
        navigation.navigate('NovaReceita');
    };


    render() {
        const {recipes} = this.state;

        return (
            <SafeAreaProvider>
                <Container>
                    <List
                        data={recipes}
                        keyExtractor={recipe => recipe.id}
                        renderItem={({item}) => (
                            <Recipe onPress={() => this.handleNavigateReceita(item)}>
                                <Name>{item.recipe_title}</Name>
                                <Icon name="arrowright" size={25} color="#585858"/>
                            </Recipe>
                        )}
                    />
                </Container>

                <Container style={{alignItems: 'center'}}>
                        <SubmitButton onPress={() => this.NovaReceita()}>
                            <Name>Nova Receita</Name>
                        </SubmitButton>
                    </Container>
            </SafeAreaProvider>
        );
    }

}
