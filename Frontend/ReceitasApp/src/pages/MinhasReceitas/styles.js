import styled from "styled-components/native";
import {Dimensions} from 'react-native';


export const Container = styled.View`
height: ${Dimensions.get('window').width / 0.65}px;
flex-direction: column;
padding: 20px;
background: #fff;
`;

export const List = styled.FlatList.attrs({
    showsVerticalScrollIndicator: false,
})``;

export const Recipe = styled.TouchableOpacity`
height: ${Dimensions.get('window').width / 10}px;
flex-direction: row;
justify-content: space-between;
padding: 0 10px;
margin-top: 15px;
border-radius: 2px;
border: 1px;
`;

export const Name = styled.Text`
font-size: 16px;
color: #565656;
font-weight: bold;
margin-top: 4px;
justifyContent: center;
text-align: left;
`;