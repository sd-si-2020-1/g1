import React, {Component} from 'react';
import {AsyncStorage, ScrollView} from 'react-native';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import {Ingredients, IngredientsInputs, Input, InputIngredients, Name, SubmitButton} from './styles';

import {Container, ContainerRow, List, Title} from '../Receita/styles'
import Icon from "react-native-vector-icons/AntDesign";
import api from "../../services/api"

Icon.loadFont();

export default class NovaLista extends Component {
    static navigationOptions = {
        title: 'NovaLista',
    };

    static initialState = {};

    state = {
        list_title: "",
        ingredients: [],
        ingredient_name: "",
        ingredient_description: "",
        ingredient_count: "",
        ingredient_unit: "",
        username: "",
        password: "",
        idLista: ""
    };

    excluirIngrediente = (name) => {
        const {ingredients} = this.state;
        let data = ingredients.filter(item => item.ingredient_name !== name);
        this.setState({
            ingredients: data
        })
    }

    addIngredient = (data) => {
        const {ingredients} = this.state;
        var ingredient = {
            ingredient_name: "",
            ingredient_description: "",
            ingredient_count: "",
            ingredient_unit: ""
        };

        this.setState(
            {
                ingredients: [...ingredients, data]
            }
        );
    };

    async componentDidMount() {
        const storage = await AsyncStorage.getItem('login');
        let data = JSON.parse(storage);

        if (!data.username || !data.password){
            const {navigation} = this.props;
            navigation.navigate('Home');
        }
        this.setState({
            username: data.username,
            password: data.password,
        })
    }

    componentDidUpdate(_, prevState) {
    }

    handleNavigate = () => {
        const {navigation} = this.props;
        navigation.navigate('Home');
    };

    async criarLista(){
        const {username, password, list_title} = this.state;

        const list = {
            username,
            password,
            list_title
        };
        let data = this.encodeObject(list);
        this.postList(data);
    }

    async postList(data) {
        const response = await api.post(`/list`, data, {'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'}).then(
            (res) => {
                try {
                    this.setState({idLista: res.data.idLista})
                    this.createIngredients()
                } catch (e) {
                    this.setState({messageError: 'Username ou Password incorretos.'})
                    this.showAlert();
                }
            });
    }

    createIngredients() {
        const { idLista, username, password, ingredients} = this.state;

        ingredients.forEach(item => {
            const ingredient = {
                username,
                password,
                ingredient_name: item.ingredient_name,
                ingredient_description: item.ingredient_description,
                ingredient_count: item.ingredient_count,
                ingredient_unit: item.ingredient_unit
            };
            let data = this.encodeObject(ingredient);

            let postStatus = this.postIngredient(data, idLista);
            if (postStatus){
            }
        });

        this.handleNavigate();
    }

    async postIngredient(data, id) {
        const response = await api.post(`/list/` + id + '/items', data, {'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'}).then(
            (res) => {
                try {

                } catch (e) {
                    this.setState({messageError: 'Sistema temporariamente Indisponível'})
                    this.showAlert();
                }
            });
        return !!response.data;
    }

    encodeObject = (object) => {
        var data = [];
        for (var property in object) {
            var encodedKey = encodeURIComponent(property);
            var encodedValue = encodeURIComponent(object[property]);
            data.push(encodedKey + "=" + encodedValue);
        }

        data = data.join("&");
        return data;
    };

    render() {
        const {
            recipe_title,
            ingredients,
        } = this.state;

        const dataInicial = {
            ingredient_name: "",
            ingredient_description: "",
            ingredient_count: "",
            ingredient_unit: ""
        };

        let data = dataInicial;

        const {navigation} = this.props;
        return (
            <SafeAreaProvider style={{backgroundColor: '#fff'}}>
                <ScrollView>
                    <ContainerRow>
                        <Input
                            autoCorrect={false}
                            autoCapitalize="none"
                            placeholder="Título Lista"
                            onChangeText={text => this.setState({list_title: text})}
                        />
                    </ContainerRow>
                    <ContainerRow>
                        <Title>
                            Ingredientes
                        </Title>
                    </ContainerRow>
                    <Container>
                        <IngredientsInputs>
                            <InputIngredients
                                autoCorrect={false}
                                autoCapitalize="none"
                                placeholder="Nome"
                                onChangeText={text => data.ingredient_name = text}
                            />
                            <InputIngredients
                                autoCorrect={false}
                                autoCapitalize="none"
                                placeholder="Unidade"
                                onChangeText={text => data.ingredient_unit = text}
                            />
                            <InputIngredients
                                autoCorrect={false}
                                autoCapitalize="none"
                                placeholder="Quantidade"
                                onChangeText={text => data.ingredient_count = text}
                            />
                        </IngredientsInputs>
                        <SubmitButton onPress={() => this.addIngredient(data)}>
                            <Name>Adicionar ingrediente</Name>
                        </SubmitButton>
                        <List
                            data={ingredients}
                            keyExtractor={(ingredient, index) => ingredient.index}
                            renderItem={({item}) => (
                                <Ingredients>
                                    <InputIngredients
                                        autoCorrect={false}
                                        autoCapitalize="none"
                                        placeholder="Nome"
                                        value={item.ingredient_name}
                                    />
                                    <InputIngredients
                                        autoCorrect={false}
                                        autoCapitalize="none"
                                        placeholder="Unidade"
                                        value={item.ingredient_unit}
                                    />
                                    <InputIngredients
                                        autoCorrect={false}
                                        autoCapitalize="none"
                                        placeholder="Quantidade"
                                        value={item.ingredient_count}
                                    />
                                    <SubmitButton onPress={() => this.excluirIngrediente(item.ingredient_name)}>

                                    </SubmitButton>
                                </Ingredients>
                            )}
                        />
                    </Container>
                    <Container style={{alignItems: 'center'}}>
                        <SubmitButton onPress={() => this.criarLista()}>
                            <Name>Salvar</Name>
                        </SubmitButton>
                    </Container>
                </ScrollView>
            </SafeAreaProvider>
        );
    }

}
