import React, {Component} from 'react';
import {AsyncStorage, ScrollView} from 'react-native';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import {
    ContainerPreparo,
    Description,
    Ingredients,
    IngredientsInputs,
    Input,
    InputIngredients,
    Name,
    SubmitButton
} from './styles';

import {Container, ContainerRow, List, Title} from '../Receita/styles'
import Icon from "react-native-vector-icons/AntDesign";
import api from "../../services/api"
import AwesomeAlert from "react-native-awesome-alerts";

Icon.loadFont();

export default class NovaReceita extends Component {
    static navigationOptions = {
        title: 'NovaReceita',
    };

    static initialState = {};

    state = {
        username: "",
        password: "",
        id: "",
        _id: "",
        _v: "",
        recipe: {},
        recipe_title: "",
        recipe_description: "",
        ingredients: [],
        ingredient_name: "",
        ingredient_description: "",
        ingredient_count: "",
        ingredient_unit: "",
        showAlert: false,
        messageError: ""
    };

    async componentDidMount() {
        const storage = await AsyncStorage.getItem('login');
        let data = JSON.parse(storage);
        this.setState({
            username: data.username,
            password: data.password,
        })
    }

    addIngredient = (data) => {
        const {ingredients} = this.state;

        this.setState(
            {
                ingredients: [...ingredients, data]
            }
        );
    }

    handleNavigate = () => {
        const {navigation} = this.props;
        navigation.navigate('Home');
    };

    excluirIngrediente = (name) => {
        const {ingredients} = this.state;
        let data = ingredients.filter(item => item.ingredient_name !== name);
        this.setState({
            ingredients: data
        })
    }

    salvarReceita() {
        const {username, password, recipe_title, recipe_description} = this.state;
        const recipe = {
            username,
            password,
            recipe_title,
            recipe_description
        };

        let data = this.encodeObject(recipe);
        this.postReceita(data);
    };

    encodeObject = (object) => {
        var data = [];
        for (var property in object) {
            var encodedKey = encodeURIComponent(property);
            var encodedValue = encodeURIComponent(object[property]);
            data.push(encodedKey + "=" + encodedValue);
        }

        data = data.join("&");
        return data;
    };

    async postReceita(data) {
        const response = await api.post(`/recipe`, data, {'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'}).then(
            (res) => {
                try {
                    this.setState({id: res.data.id, _id: res.data._id, _v: res.data._v})
                    this.createIngredients()
                } catch (e) {
                    this.setState({messageError: 'Username ou Password incorretos.'})
                    this.showAlert();
                }
            });
    }

    createIngredients() {
        const {id, username, password, ingredients} = this.state;

        ingredients.forEach(item => {
            const ingredient = {
                username,
                password,
                ingredient_name: item.ingredient_name,
                ingredient_description: item.ingredient_description,
                ingredient_count: item.ingredient_count,
                ingredient_unit: item.ingredient_unit
            };
            let data = this.encodeObject(ingredient);

            let postStatus = this.postIngredient(data, id);
            if (postStatus){
            }
        });


        this.saveRecipeStorage()

    }

    async saveRecipeStorage(){
        const {id, username, _id, _v, ingredients, recipe_title, recipe_description} = this.state;
        let recipe = {
            id: id,
            _id: _id,
            _v: _v,
            username: username,
            recipe_title: recipe_title,
            recipe_description: recipe_description,
            recipe_ingredients: ingredients};

        let storage = await AsyncStorage.getItem('minhasReceitas');

        storage = storage ? JSON.parse(storage) : [];
        storage.push(recipe);
        await AsyncStorage.setItem('minhasReceitas', JSON.stringify(storage)).then(this.handleNavigate());
    }

    async postIngredient(data, id) {
        const response = await api.post(`/recipe/` + id + '/ingredients', data, {'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'}).then(
            (res) => {
                try {

                } catch (e) {
                    this.setState({messageError: 'Sistema temporariamente Indisponível'})
                    this.showAlert();
                }
            });
        return !!response.data;
    }

    showAlert = () => {
        this.setState({
            showAlert: true
        });
    };

    hideAlert = () => {
        this.setState({
            showAlert: false
        });
    };

    render() {
        const {
            recipe,
            ingredients,
            showAlert,
            messageError
        } = this.state;

        const data = {
            ingredient_name: "",
            ingredient_description: "",
            ingredient_count: "",
            ingredient_unit: ""
        };

        return (
            <SafeAreaProvider style={{backgroundColor: '#fff'}}>
                <ScrollView>
                    <ContainerRow>
                        <Input
                            autoCorrect={false}
                            autoCapitalize="none"
                            placeholder="Título Receita"
                            onChangeText={text => this.setState({recipe_title: text})}
                        />
                    </ContainerRow>
                    <ContainerRow>
                        <Title>
                            Ingredientes
                        </Title>
                    </ContainerRow>
                    <Container>
                        <IngredientsInputs>
                            <InputIngredients
                                autoCorrect={false}
                                autoCapitalize="none"
                                placeholder="Nome"
                                onChangeText={text => data.ingredient_name = text}
                            />
                            <InputIngredients
                                autoCorrect={false}
                                autoCapitalize="none"
                                placeholder="Unidade"
                                onChangeText={text => data.ingredient_unit = text}
                            />
                            <InputIngredients
                                autoCorrect={false}
                                autoCapitalize="none"
                                placeholder="Quantidade"
                                onChangeText={text => data.ingredient_count = text}
                            />
                        </IngredientsInputs>
                        <SubmitButton onPress={() => this.addIngredient(data)}>
                            <Name>Adicionar ingrediente</Name>
                        </SubmitButton>
                        <List
                            data={ingredients}
                            keyExtractor={(ingredient, index) => ingredient.index}
                            renderItem={({item}) => (
                                <Ingredients>
                                    <InputIngredients
                                        autoCorrect={false}
                                        autoCapitalize="none"
                                        placeholder="Nome"
                                        value={item.ingredient_name}
                                        editable={false}
                                    />
                                    <InputIngredients
                                        autoCorrect={false}
                                        autoCapitalize="none"
                                        placeholder="Unidade"
                                        value={item.ingredient_unit}
                                        editable={false}
                                    />
                                    <InputIngredients
                                        autoCorrect={false}
                                        autoCapitalize="none"
                                        placeholder="Quantidade"
                                        value={item.ingredient_count}
                                        editable={false}
                                    />
                                    <SubmitButton onPress={() => this.excluirIngrediente(item.ingredient_name)}>

                                    </SubmitButton>
                                </Ingredients>
                            )}
                        />
                    </Container>
                    <ContainerRow>
                        <Title>
                            Modo de preparo
                        </Title>
                    </ContainerRow>
                    <ContainerPreparo>
                        <Description
                            autoCorrect={false}
                            autoCapitalize="none"
                            placeholder="Modo de preparo"
                            onChangeText={text => this.setState({recipe_description: text})}
                        />
                    </ContainerPreparo>
                    <Container style={{alignItems: 'center'}}>
                        <SubmitButton onPress={() => this.salvarReceita()}>
                            <Name>Salvar</Name>
                        </SubmitButton>
                    </Container>
                </ScrollView>
                <AwesomeAlert
                    show={showAlert}
                    showProgress={false}
                    title="Error"
                    message={messageError}
                    closeOnTouchOutside={true}
                    closeOnHardwareBackPress={false}
                    showCancelButton={false}
                    showConfirmButton={true}
                    confirmText="Entendi"
                    confirmButtonColor="#FCCA17"
                    onCancelPressed={() => {
                        this.hideAlert();
                    }}
                    onConfirmPressed={() => {
                        this.hideAlert();
                    }}
                />
            </SafeAreaProvider>
        );
    }

}

