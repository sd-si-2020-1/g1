import React, {Component} from 'react';
import {StackActions} from '@react-navigation/native';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import {
    ContainerLogin,
    Form,
    Input,
    Name,
    PlaceHolder,
    SubmitButton,
    ImageProfile,
    ContainerImage,
    LogoutButton,
    ContainerLogout,
    ProfileText
} from './styles';
import imgProfile from '../../assets/profile.png';
import {AsyncStorage, Platform} from "react-native";

export default class Profile extends Component {
    static navigationOptions = {
        title: 'Profile',
    };

    static initialState = {};

    state = {
        username: '',
        password: '',
    };

    async componentDidMount() {
        const login = await AsyncStorage.getItem('login');
        let data = JSON.parse(login);
        this.setState({
            username: data.username,
            password: data.password,
        })
    }

    componentDidUpdate(_, prevState) {

    }

    handleNavigate = () => {
        const {navigation} = this.props;
        navigation.dispatch(
            StackActions.replace('Login')
        );
    };

    render() {
        const {
            username,
            password,
        } = this.state;

        return (
            <SafeAreaProvider
                style={{flex: 1, paddingTop: Platform.OS === 'android' ? 25 : 0, backgroundColor: '#FCCA17'}}>
                <ContainerLogout>
                    <ProfileText>
                        Meu Perfil
                    </ProfileText>
                    <LogoutButton onPress={() => this.handleNavigate()}>
                        <Name>Sair</Name>
                    </LogoutButton>
                </ContainerLogout>
                <ContainerImage>
                    <ImageProfile source={imgProfile}/>
                </ContainerImage>
                <ContainerLogin>
                    <Form>
                        <PlaceHolder>Username</PlaceHolder>
                        <Input
                            autoCorrect={false}
                            autoCapitalize="none"
                            placeholder=""
                            value={username}
                            editable={false}
                            onChangeText={text => this.setState({username: text})}
                        />
                        <PlaceHolder>Password</PlaceHolder>
                        <Input
                            autoCorrect={false}
                            autoCapitalize="none"
                            placeholder=""
                            value={password}
                            editable={false}
                            onChangeText={text => this.setState({password: text})}
                        />
                    </Form>
                </ContainerLogin>
            </SafeAreaProvider>
        );
    }

}
