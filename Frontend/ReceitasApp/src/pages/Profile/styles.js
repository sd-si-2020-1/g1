import styled from "styled-components/native";
import {RectButton} from "react-native-gesture-handler";
import {Dimensions} from 'react-native';


export const ImageProfile = styled.Image.attrs({
    resizeMode: 'cover'
})`
borderRadius: 1000px;
height: 160px;
width: 160px;
`;

export const ContainerImage = styled.View`
flex-direction: column;
justify-content: center;
align-items: center;
background: #fff;
`;
export const ContainerLogin = styled.View`
flex: 1;
flex-direction: column;
padding: 0 20px;
background: #fff;
`;

export const Form = styled.View`
flex: 1;
flex-direction: column;
border-bottom-width: 1px;
border-color: #eee
`;

export const Input = styled.TextInput.attrs({
    placeholderTextColor: '#535353',
})`
font-size: 22px;
margin: 10px 10px 20px;
justify-content: center;
align-items: center;
color: #434343;
font-weight: bold;
borderBottomWidth: 1px;
`;

export const SubmitButton = styled(RectButton)`
justify-content: center;
align-items: center;
height: 40px;
background: #FCCA17;
border-radius: 4px;
padding: 0 12px;
`;

export const ContainerLogout = styled.View`
flex-direction: row;
align-items: center;
justify-content: space-between;
background: #fff;
`;
export const LogoutButton = styled(RectButton)`
justify-content: center;
align-items: center;
height: 40px;
width: 80px;
margin: 0 20px;
background: #FCCA17;
border-radius: 4px;
padding: 0 12px;
`;
export const ProfileText = styled.Text`
font-size: 22px;
width: 160px;
margin: 20px;
justify-content: center;
align-items: center;
color: #434343;
font-weight: bold;
`;

export const Recipe = styled.TouchableOpacity`
height: ${Dimensions.get('window').width / 2}px;
width: ${Dimensions.get('window').width / 2}px;
margin-top: 15px;
margin-right: 1px;
margin-left: 1px;
flex: 1;
border-radius: 2px;
elevation: 2;
`;

export const Name = styled.Text`
font-size: 16px;
color: #565656;
font-weight: bold;
margin-top: 4px;
text-align: center;
`;

export const PlaceHolder = styled.Text`
font-size: 16px;
color: #565656;
font-weight: bold;
margin-top: 4px;
`;

export const IngredientName = styled.Text`
font-size: 14px;
color: #565656;
font-weight: bold;
margin-top: 4px;
text-align: center;
`;

export const Description = styled.Text.attrs({
    numberOfLines: 2
})`
font-size: 15px;
line-height: 18px;
color: #565656;
margin-top: 5px;
text-align: center;
`;

export const IngredientDescription = styled.Text.attrs({
    numberOfLines: 2
})`
font-size: 13px;
line-height: 18px;
color: #565656;
margin-top: 5px;
text-align: center;
`;

export const RecipeButton = styled(RectButton)`
margin-top: 10px;
margin-bottom: 10px;
align-self: stretch;
border-radius: 4px;
background: transparent;
justify-content: center;
align-items: center;
height: 36px;
`;

export const SomarZerarButton = styled(RectButton)`
margin-top: 10px;
align-self: stretch;
border-radius: 4px;
background: #f48024;
justify-content: center;
align-items: center;
height: 36px;
`;

export const RecipeButtonText = styled.Text`
font-size: 14px;
font-weight: bold;
color: #000000;
text-transform: uppercase;
`;
