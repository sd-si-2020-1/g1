import React, {Component} from 'react';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import {ContainerLogin, Form, Input, Name, PlaceHolder, SubmitButton} from './styles';
import {StackActions} from '@react-navigation/native';
import {AsyncStorage, ScrollView} from "react-native";
import api from "../../services/api";

export default class SignUp extends Component {
    static navigationOptions = {
        title: 'SignUp',
    };

    static initialState = {};

    state = {
        username: '',
        password: '',
        confirmPass: '',
    };

    async componentDidMount() {
        const {user} = this.state;

        // const response = await api.get(`/recipe/4`);
        // var data = null;
        //
        // console.log("response \n" + response);
        //
        // data = response.data[0];

        var data = [];
        //
        // data = {
        //     _id: response.data[0].id,
        //     username: response.data[0].username,
        //     recipe_title: response.data[0].recipe_title,
        //     recipe_description: response.data[0].recipe_description,
        //     recipe_ingredients: response.data[0].recipe_ingredients,
        // };

        this.setState({
            recipe: data,
        })


    }

    componentDidUpdate(_, prevState) {

    }

    signUp () {
        const {username, password, confirmPass} = this.state;
        if (password === confirmPass){
            var details = {
                'username': username,
                'password': password,
            };

            var data = [];
            for (var property in details) {
                var encodedKey = encodeURIComponent(property);
                var encodedValue = encodeURIComponent(details[property]);
                data.push(encodedKey + "=" + encodedValue);
            }

            data = data.join("&");

            this.postSignUp(data);
        }

    }

    async postSignUp(data) {
        const {navigation} = this.props;
        const response = await api.post(`/signUp`, data, {'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'});
        if (response.data){
            try {
                navigation.dispatch(
                    StackActions.replace('Home')
                );
            } catch (e) {
                console.log(e);
            }
        }else{

        }
    }

    render() {
        const {
            nome,
            sobrenome,
            email,
            password,
            confirmPass
        } = this.state;
        const {navigation} = this.props;

        return (
            <SafeAreaProvider>
                <ScrollView>
                    <ContainerLogin>
                        <Form>
                            <PlaceHolder>Username</PlaceHolder>
                            <Input
                                autoCorrect={false}
                                autoCapitalize="none"
                                placeholder=""
                                value={nome}
                                onChangeText={text => this.setState({username: text})}
                            />
                            <PlaceHolder>Senha</PlaceHolder>
                            <Input
                                autoCorrect={false}
                                autoCapitalize="none"
                                placeholder=""
                                value={password}
                                onChangeText={text => this.setState({password: text})}
                            />
                            <PlaceHolder>Confirme a senha</PlaceHolder>
                            <Input
                                autoCorrect={false}
                                autoCapitalize="none"
                                placeholder=""
                                value={confirmPass}
                                onChangeText={text => this.setState({confirmPass: text})}
                            />
                            <SubmitButton onPress={() => this.signUp()}>
                                <Name>Sign-up</Name>
                            </SubmitButton>
                        </Form>
                    </ContainerLogin>
                </ScrollView>
            </SafeAreaProvider>
        );
    }

}
