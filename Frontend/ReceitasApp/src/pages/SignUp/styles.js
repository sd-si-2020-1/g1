import styled from "styled-components/native";
import { RectButton } from "react-native-gesture-handler";
import { Dimensions } from 'react-native';
import logo from '../../assets/g1-logo.png';

export const ContainerLogin = styled.View`
flex: 1;
flex-direction: column;
padding: 20px;
background: #fff;
`;

export const Form = styled.View`
flex: 1;
flex-direction: column;
border-bottom-width: 1px;
border-color: #eee
`;

export const Input = styled.TextInput.attrs({
    placeholderTextColor: '#535353',
})`
height:  ${Dimensions.get('window').height/28}px;
background: #fff;
border-radius: 4px;
padding: 0 15px;
margin-bottom: 5px;
borderBottomWidth: 1.5px;
`;

export const SubmitButton = styled(RectButton)`
justify-content: center;
align-items: center;
height: 40px;
background: #FCCA17;
border-radius: 4px;
padding: 0 12px;
`;

export const Recipe = styled.TouchableOpacity`
height: ${Dimensions.get('window').width/2}px;
width: ${Dimensions.get('window').width/2}px;
margin-top: 15px;
margin-right: 1px;
margin-left: 1px;
flex: 1;
border-radius: 2px;
elevation: 2;
`;

export const Name = styled.Text`
font-size: 16px;
color: #565656;
font-weight: bold;
margin-top: 4px;
text-align: center;
`;

export const PlaceHolder = styled.Text`
font-size: 16px;
color: #565656;
font-weight: bold;
margin-top: 4px;
`;

