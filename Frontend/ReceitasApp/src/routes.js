import * as React from 'react';
import {Dimensions} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import Main from './pages/Main/Index'
import Login from './pages/Login/Index'
import SignUp from './pages/SignUp/Index'
import Profile from './pages/Profile/Index'
import Receita from './pages/Receita/Index'
import MinhasReceitas from "./pages/MinhasReceitas/Index";
import Historico from "./pages/Historico/Index";
import Favoritos from "./pages/Favoritos/Index";
import MinhasListas from "./pages/MinhasListas/Index";
import NovaReceita from "./pages/NovaReceita/Index";
import NovaLista from "./pages/NovaLista/Index";
import EditarLista from "./pages/EditarLista/Index";
import ListaIngredientes from "./pages/ListaIngredientes/Index";
import {combineReducers, createStore} from 'redux';
import Contador from './pages/Contador/contador';
import contadorReducer from './components/contadorReducer';
import {Provider} from 'react-redux';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Logo from './components/logoLogin';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';

Icon.loadFont();

const isLogado = false;
let initialRoute = isLogado ? 'Home' : 'Login';
const Stack = createStackNavigator();
const reducers = combineReducers({
    contadorReducerChave: contadorReducer
});

let store = createStore(reducers);


function Routes() {
    return (
        <Provider store={store}>
            <NavigationContainer>
                <Stack.Navigator initialRouteName={initialRoute}>
                    <Stack.Screen name="Login"
                                  component={LoginTabs}
                                  options={{
                                      headerTitle: <Logo/>,
                                      headerLeft: null,
                                      headerStyle: {
                                          height: Dimensions.get('window').height / 2.5,
                                          backgroundColor: '#FCCA17',
                                          borderRadius: 20,
                                      }, headerTitleStyle: {
                                          alignSelf: 'center',
                                          justifyContent: 'center',
                                      }
                                  }}/>
                    <Stack.Screen name="Home"
                                  component={Home}
                                  options={{
                                      headerLeft: null,
                                      headerShown: false,
                                      gestureEnabled: false
                                  }}/>
                    <Stack.Screen name="MinhasReceitas"
                                  component={MinhasReceitas}
                                  options={{
                                      headerTitle: "Minhas Receitas",
                                      headerStyle: {
                                          backgroundColor: '#FCCA17',
                                      }
                                  }}/>
                    <Stack.Screen name="MinhasListas"
                                  component={MinhasListas}
                                  options={{
                                      headerTitle: "Minhas Listas",
                                      headerStyle: {
                                          backgroundColor: '#FCCA17',
                                      }
                                  }}/>
                    <Stack.Screen name="Receita"
                                  component={Receita}
                                  options={{
                                      headerStyle: {
                                          backgroundColor: '#FCCA17',
                                      }
                                  }}/>
                    <Stack.Screen name="NovaReceita"
                                  component={NovaReceita}
                                  options={{
                                      headerTitle: "Nova Receita",
                                      headerStyle: {
                                          backgroundColor: '#FCCA17',
                                      }
                                  }}>

                    </Stack.Screen>
                    <Stack.Screen name="NovaLista"
                                  component={NovaLista}
                                  options={{
                                      headerTitle: "Nova Lista",
                                      headerStyle: {
                                          backgroundColor: '#FCCA17',
                                      }
                                  }}/>
                    <Stack.Screen name="ListaIngredientes"
                                  component={ListaIngredientes}
                                  options={{
                                      headerTitle: "Lista Ingredientes",
                                      headerStyle: {
                                          backgroundColor: '#FCCA17',
                                      }
                                  }}/>
                    <Stack.Screen name="EditarLista"
                                  component={EditarLista}
                                  options={{
                                      headerTitle: "Editar Lista",
                                      headerStyle: {
                                          backgroundColor: '#FCCA17',
                                      }
                                  }}/>
                    <Stack.Screen name={Contador.navigationOptions.title} component={Contador}/>
                </Stack.Navigator>
            </NavigationContainer>
        </Provider>
    );
}

const Tab = createBottomTabNavigator();

function Home() {

    return (
        <Tab.Navigator
            screenOptions={({route}) => ({
                tabBarIcon: ({focused, color, size}) => {
                    let iconName;

                    switch (route.name) {
                        case "Home":
                            iconName = 'home';
                            break;
                        case "Favoritos":
                            iconName = 'star';
                            break;
                        case "Profile":
                            iconName = 'person';
                            break;
                        case "Historico":
                            iconName = 'history';
                            break;
                        default:
                            iconName = 'home';
                            break;
                    }

                    return <Icon name={iconName} size={32} color={color}/>;
                },
            })}
            tabBarOptions={{
                activeTintColor: '#FCCA17',
                inactiveTintColor: '#000',
                showLabel: false,
                fontSize: 20,
                style: {
                    backgroundColor: '#fff',
                },
                tabStyle: {}
            }}
        >
            <Tab.Screen name="Home" component={Main}/>
            <Tab.Screen name="Favoritos" component={Favoritos}/>
            <Tab.Screen name="Profile" component={Profile}/>
            <Tab.Screen name="Historico" component={Historico}/>
        </Tab.Navigator>
    );
}

const TopTab = createMaterialTopTabNavigator();

function LoginTabs() {
    return (
        <TopTab.Navigator
            tabBarOptions={{
                inactiveTintColor: '#000',
                activeTabStyle: {color: '#FCCA17'},
                indicatorStyle: {
                    backgroundColor: '#FCCA17',
                    height: '80%',
                    borderRadius: 15,
                    marginBottom: 8,
                    marginLeft: 12,
                    width: '45%'
                },
                style: {
                    justifyContent: 'center',
                    backgroundColor: '#ffffff',
                    borderRadius: 15,
                    margin: 5,
                    height: 60,
                },
                fontSize: 20,
            }}>
            <Tab.Screen name="LoginTab" component={Login} options={{
                title: 'Login',
            }}/>
            <Tab.Screen name="SignUpTab" component={SignUp} options={{
                title: 'Sign-Up'
            }}/>
        </TopTab.Navigator>
    );
}

export default Routes;