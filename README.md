# G1 - RECEITAS

Este é um projeto de desenvolvimento de um **aplicativo de receitas** realizado na disciplina de Sistemas Distribuídos - 2020/1, mediante a orientação do professor Marcelo Akira. O projeto foi dividido em duas etapas, a primeira, construção do [backend](/Backend/README.md) e, a segunda, contrução do [frontend](/Frontend/README.md).

# Sumário
- [Motivação do projeto](#Motivação-do-projeto)
- [Integrantes e suas responsabilidades](#Integrantes-e-suas-responsabilidades)
- [Requisitos organizacionais](#Requisitos-organizacionais)
- [Requisitos técnicos](#Requisitos-técnicos)
- [Instalação e Execução](#Instalação-e-Execução)
- [Documentação](#Documentação)
- [Relatório Técnico](#Relatório-Técnico)
- [Contato](#Contato)


# Motivação do projeto

A atual situação de pandemia está levando mais pessoas a cozinhar em casa. Isso está impulsionando sites e aplicativos de receitas. O portal de receitas do UOL teve crescimento de 230% da audiência em junho quando comparado a março. Já o canal do programa MasterChef do YouTube ganhou 129 mil novas inscrições durante o período de quarentena, com crescimento de de 51% nas visualizações ([fonte](https://tab.uol.com.br/noticias/redacao/2020/07/03/chefs-na-quarentena-como-a-pandemia-esta-afetando-os-habitos-culinarios.htm)).

Isso motiva o nosso projeto, que é desenvolver um aplicativo de receitas. Partimos da hipótese que os aplicativos atualmente no mercado não são suficientes para lidar com a demanda atual, deixando lacunas que podem ser exploradas. 

No artigo técnico, iremos identificar os principais pontos fracos dos aplicativos presentes no mercado, principalmente a partir da leitura de reviews na Play Store. Em seguida, iremos desenvolver o back-end e o front-end do projeto, buscando superar os aplicativos concorrentes nos pontos fracos identificados.


# Integrantes e suas responsabilidades

- [Ruan Chaves Rodrigues](https://gitlab.com/ruanchaves) 
  - **Líder do projeto**
  - Desenvolvimento back-end

- [Victor Alexandre de Carvalho Coelho](https://gitlab.com/victor.fisica01)
  - Testes e automatização de testes
  - Documentação
  - Revisão

- [Pedro Costa e Silva Faustino](https://gitlab.com/pedrofaustino01)
  - Desenvolvimento front-end
  - Documentação

- [Lucas de Oliveira Naves](https://gitlab.com/LucasNaves21)
  - Documentação
  - Design do front-end

- [Marcelo Akira Inuzuka](https://gitlab.com/marceloakira)
  - Mentoria

# Requisitos organizacionais

* **Não faça commit direto no master**, abra uma branch, faça commits na branch, e depois abra um pull request para dar merge da sua branch no master.

* **Commits só serão aceitos se eles passarem nos testes de integração automatizados**. 

* **Todo pull request deve referenciar uma issue**.

* [Enviar no fórum no Turing](https://turing.inf.ufg.br/mod/forum/view.php?id=11010) relatórios semanais (por sprint). Basta explicar brevemente o que fez referenciando as issues que resolveu pelo título e número.

# Requisitos técnicos

* Back-end: [Node](https://nodejs.org/en/)

* Documentação da API: Open API com [Swagger](https://swagger.io/)

* Banco de dados: [MongoDB](https://www.mongodb.com/)

* Testes automatizados: [Jest](https://jestjs.io/)

* Front-end: [React Native](https://reactnative.dev/)

* Wireframes e mockups: [Figma](https://www.figma.com/)



# Instalação e Execução
Siga os passos abaixo para executar a aplicação em seu smartphone físico.

Primeiramente, tenha instalado em sua máquina o [Node](https://nodejs.org/en/) e [MongoDB](https://www.mongodb.com/1) e siga os passos abaixo. Instruções para execução usando o Docker estão disponíveis em [backend](/Backend/README.md).


Inicialize o backend:
```
git clone https://gitlab.com/sd-si-2020-1/g1.git

cd g1/Backend/app

npm install

npm start
```

Antes de inicializar o frontend, certifique-se de colocar o seu ip na url base do axios em [Frontend/ReceitasApp/src/services/api.js](Frontend/ReceitasApp/src/services/api.js). A porta usada deve ser a 3000, ou seja, no final a estrutura será **http://{seu ip}:3000**

![](https://media.discordapp.net/attachments/802308639083462716/802310267383054336/unknown.png)


Inicialize o frontend:
```
cd g1/Frontend/ReceitasApp

npm install

expo start
```
Após a execução do comando expo start, será mostrado um QR code no terminal e também em uma janela de seu navegador padrão. 

![alt text](https://miro.medium.com/max/3820/1*N7AWqIVrSJtDjiPS4se4pg.png)

Faça a leitura do QR code com o aplicativo do Expo client disponível na [**AppStore**](https://apps.apple.com/br/app/expo-client/id982107779) ou na [**Playstore**](https://play.google.com/store/apps/details?id=host.exp.exponent&hl=pt_BR&gl=US).

Feito a leitura, o nosso aplicativo será executado em seu smartphone através do Expo client. 

Se você não tiver um smartphone físico é possível executar a aplicação em um emulador. Para isso verifique as instruções na [documentação oficial do Expo](https://docs.expo.io/workflow/android-studio-emulator/). 


# Documentação
A documentação mais detalhada do projeto se encontra nas etapas específicas do projeto: [backend](/Backend/README.md) e [frontend](/Frontend/README.md).


# Relatório Técnico
A descrição de como o projeto foi desenvolvido se encontra no **relatório técnico** disponível no [**Overleaf**](https://www.overleaf.com/read/bqkrrnghycfz).

# Contato

Qualquer dúvida ou sugestão, entre em contado com os [integrantes do projeto](#Integrantes-e-suas-responsabilidades), ou abra uma nova [issue](https://gitlab.com/sd-si-2020-1/g1/-/issues). 
